export const trainingPlane = [
  { state: 0, zone: 0, status: '', interval: 10, experience: 0, summtime: 0 },
  { state: 1, zone: 1, status: 'Бронзовый', interval: 20, experience: 1, summtime: 20 },
  { state: 2, zone: 1, status: 'Бронзовый', interval: 30, experience: 2, summtime: 50 },
  { state: 3, zone: 1, status: 'Бронзовый', interval: 40, experience: 3, summtime: 90 },
  { state: 4, zone: 1, status: 'Бронзовый', interval: 50, experience: 4, summtime: 140 },
  { state: 5, zone: 1, status: 'Бронзовый', interval: 60, experience: 5, summtime: 200 },
  { state: 6, zone: 2, status: 'Серебряный', interval: 120, experience: 10, summtime: 320 },
  { state: 7, zone: 2, status: 'Серебряный', interval: 240, experience: 15, summtime: 560 },
  { state: 8, zone: 2, status: 'Серебряный', interval: 360, experience: 20, summtime: 920 },
  { state: 9, zone: 2, status: 'Серебряный', interval: 480, experience: 25, summtime: 1400 },
  { state: 10, zone: 2, status: 'Серебряный', interval: 600, experience: 30, summtime: 2000 },
  { state: 11, zone: 3, status: 'Золотой', interval: 1200, experience: 50, summtime: 3200 },
  { state: 12, zone: 3, status: 'Золотой', interval: 1200, experience: 60, summtime: 4400 },
  { state: 13, zone: 3, status: 'Золотой', interval: 1200, experience: 70, summtime: 5600 },
  { state: 14, zone: 3, status: 'Золотой', interval: 2400, experience: 80, summtime: 8000 },
  { state: 15, zone: 3, status: 'Золотой', interval: 2400, experience: 90, summtime: 10400 },
  { state: 16, zone: 3, status: 'Золотой', interval: 100000000, experience: 100, summtime: 10400 }
]