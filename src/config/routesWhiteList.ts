export default [
  '/api/auth/google',
  '/api/auth/google/login',
  '/api/auth/google/register',
  '/api/auth/facebook',
  '/api/auth/facebook/login',
  '/api/auth/facebook/register',
  '/api/auth/vkontakte',
  '/api/auth/vkontakte/login',
  '/api/auth/vkontakte/register',
  '/api/users/signin',
  '/api/users/register',
  '/api/users/login',
  '/api/users/emailreset',
  '/api/users/admin/auth',
  '/audio'
];