import dotenv from 'dotenv'
import * as pgStringParser from 'pg-connection-string'
import { Dialect } from 'sequelize/types'

dotenv.config()

const herokuData = process.env.DATABASE_URL
  ? pgStringParser.parse(process.env.DATABASE_URL)
  : { database: '', user: '', password: '', host: '' }

const dialOptions = process.env.DATABASE_URL
  ? { ssl: { rejectUnauthorized: false }}
  : {}

export const env = {
  app: {
    port: process.env.APP_PORT,
    adress: process.env.APP_ADRESS,
  },
  db: {
    database: herokuData.database || process.env.DB_NAME,
    username: herokuData.user || process.env.DB_USERNAME,
    password: herokuData.password || process.env.DB_PASSWORD,
    host: herokuData.host || process.env.DB_HOST,
    port:
      <number>(<unknown>herokuData.port) ||
      <number>(<unknown>process.env.DB_PORT),
    dialect: <Dialect>process.env.DB_DIALECT || 'postgres',
    ssl: true,
    dialectOptions: dialOptions,
    logging: false,
  },
  smtp: {
    smtpUser: process.env.SMTP_USER
    // smtpPass: process.env.SMTP_PASS,
    // smtpClient: process.env.SMTP_CLIENT,
    // smtpSecret: process.env.SMTP_SECRET,
    // smtpToken: process.env.SMTP_TOKEN
  },
  admin: {
    adminName: process.env.SYSTEM_ADMIN_NAME,
    adminPWD: process.env.SYSTEM_ADMIN_PWD
  },
  google: {
    clientGoogleId: process.env.GOOGLE_ID,
    clientGoogleSecret: process.env.GOOGLE_SECRET
  },
  aws: {
    accessKeyId: process.env.AMAZON_ID,
    secretAccessKey: process.env.AMAZON_SECRET,
    bucketName: process.env.AMAZON_FOLDER
  },
  facebook: {
    clientFacebookId: process.env.FACEBOOK_ID,
    clientFacebookSecret: process.env.FACEBOOK_SECRET
  },
  vkontakte: {
    clientVkId: process.env.VK_ID,
    clientVkSecret: process.env.VK_SECRET
  },
  polly: {
    clientPollyId: process.env.POLLY_ID,
    clientPollySecret: process.env.POLLY_SECRET
  },
  filesize: process.env.MAX_FILE_SIZE,
  port: process.env.PORT,
  use_env_variable: process.env.DATABASE_URL,
  crypto_key: process.env.CR_KEY,
}

export const { database, username, password, host, port, dialect, ssl, dialectOptions, logging} = env.db;
