import { Router } from 'express';
import * as logsService from '../services/logsService';

const router = Router();

router
  .get('/full', (req, res, next) => logsService.getFullLogs(req.query)
    .then(logs => res.send(logs))
    .catch(next))
  .get('/', (req, res, next) => logsService.getLogs(req.query)
    .then(logs => res.send(logs))
    .catch(next))
  .post('/emptylogs', (req, res, next) => logsService.emptyLogs(req.body)
    .then(delLog => res.send(delLog))
    .catch(next))
  .post('/', (req, res, next) => logsService.createLog(req)
    .then(newLog => res.send(newLog))
    .catch(next))
  .put('/', (req, res, next) => logsService.updateLog(req)
    .then((updLog) => res.send(updLog))
    .catch(next))
  .delete('/userlogs/:userId', (req, res, next) => logsService.deleteUserLogs(req.params.userId)
    .then(delLog => res.send(delLog))
    .catch(next))
  .delete('/wordlogs/:wordId', (req, res, next) => logsService.deleteWordLogs(req.params.wordId)
    .then(delLog => res.send(delLog))
    .catch(next))
  .delete('/:id', (req, res, next) => logsService.deleteLog(req.params.id)
    .then(delLog => res.send(delLog))
    .catch(next));

export default router;
