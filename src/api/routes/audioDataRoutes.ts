import { Router } from 'express';
import * as audioService from '../services/audioService';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .get('/voices/:id', (req, res, next) => audioService.loadVoicesById(req, res)
    .catch(next))
  .get('/voices', (req, res, next) => audioService.loadVoices(req, res)
    .catch(next))
  .post('/', fileMiddleware, (req, res, next) => audioService.postAudio(req, res)
    .catch(next))    
  .put('/', (req, res, next) => audioService.updateVoice(req.body)
    .then(result => res.status(200).send(result))
    .catch(next))
  .delete('/:id', (req, res, next) => audioService.deleteVoice(req.params.id)
    .then(result => res.status(200).send(result))
    .catch(next));

export default router;
