import { Router } from 'express';
import * as audioService from '../services/audioService';

const router = Router();

router
  .get('/:id', (req, res, next) => audioService.loadFile(req.params.id)
    .then(result => res.status(200).send(result))
    .catch(next));

export default router;
