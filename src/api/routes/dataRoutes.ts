import { Router } from 'express';
import { IRequest } from '../interfaces/interfaces';
import * as dataService from '../services/dataService';

const router = Router();

router
  .get('/length', (req, res, next) => dataService.getDataLength(req.query)
    .then(result => res.status(200).send(result))
    .catch(next))
  // .get('/packet', (req, res, next) => dataService.getDataPacket((<IRequest> req).user)
  //   .then(result => res.status(200).send(result))
  //   .catch(next))
  .get('/statistic', (req, res, next) => dataService.getStatistic(<IRequest> req)
    .then(result => res.status(200).send(result))
    .catch(next))
  .get('/statcount', (req, res, next) => dataService.getStatCount(<IRequest> req)
    .then(result => res.status(200).send(result))
    .catch(next))
  .get('/commonlist', (req, res, next) => dataService.getCommonList((<IRequest> req).user.id)
    .then(result => res.status(200).send(result))
    .catch(next))
  .get('/vocabulary', (req, res, next) => dataService.getVocabulary((<IRequest> req).user)
    .then(result => res.status(200).send(result))
    .catch(next))
  .get('/list', (req, res, next) => dataService.getDataList((<IRequest> req).user, <string> req.query.count)
    .then(result => res.status(200).send(result))
    .catch(next))
  .delete('/reset', (req, res, next) => dataService.resetUserLogs((<IRequest> req).user)
    .then(result => res.status(200).send(result))
    .catch(next));

export default router;
