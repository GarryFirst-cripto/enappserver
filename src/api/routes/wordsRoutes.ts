import { Router } from 'express';
import * as wordsService from '../services/wordsServices';
import { IRequest } from '../interfaces/interfaces';

const router = Router();

router
  .get('/control', (_req, res, next) => wordsService.performControl()
    .then(result => res.send(result))
    .catch(next))
  .get('/:id', (req, res, next) => wordsService.getWordById(<IRequest> <unknown> req)
    .then(word => res.send(word))
    .catch(next))
  .get('/', (req, res, next) => wordsService.getWords(req.query)
    .then(words => res.send(words))
    .catch(next))
  .post('/statuslist', (req, res, next) => wordsService.getWordsByStatus(<IRequest> <unknown> req)
    .then(words => res.send(words))
    .catch(next))
  .post('/new', (req, res, next) => wordsService.getNewWords(<IRequest> <unknown> req)
    .then(words => res.send(words))
    .catch(next))
  .post('/list', (req, res, next) => wordsService.getWordsList(<IRequest> <unknown> req)
    .then(words => res.send(words))
    .catch(next))
  .post('/complex/:mode', (req, res, next) => wordsService.getWordsComplex(<IRequest> <unknown> req)
    .then(words => res.send(words))
    .catch(next))
  .post('/complex', (req, res, next) => wordsService.getWordsComplex(<IRequest> <unknown> req)
    .then(words => res.send(words))
    .catch(next))
  .post('/words', (req, res, next) => wordsService.createWord(req.body)
    .then(newWord => res.send(newWord))
    .catch(next))
  .post('/', (req, res, next) => wordsService.create(req.body)
    .then(newWord => res.send(newWord))
    .catch(next))
  .put('/', (req, res, next) => wordsService.updateWord(req.body)
    .then((updWord) => res.send(updWord))
    .catch(next))
  .delete('/:id', (req, res, next) => wordsService.deleteWord(req.params.id)
    .then(delWord => res.send(delWord))
    .catch(next));

export default router;
