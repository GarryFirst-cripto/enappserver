import { Router } from 'express';
import * as groupTagService from '../services/groupTagService';

const router = Router();

router
  .get('/', (req, res, next) => groupTagService.getGroupTags()
    .then(hint => res.send(hint))
    .catch(next))
  .post('/', (req, res, next) => groupTagService.create(req.body)
    .then(hint => res.send(hint))
    .catch(next))
  .put('/', (req, res, next) => groupTagService.update(req.body)
    .then((hint) => res.send(hint))
    .catch(next))
  .delete('/:id', (req, res, next) => groupTagService.deleteGroupTag(req.params.id)
    .then(item => res.send(item))
    .catch(next));

export default router;
