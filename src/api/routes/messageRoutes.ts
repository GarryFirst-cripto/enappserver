import { Router } from 'express';
import * as messageService from '../services/messageService';
import { IRequest } from '../interfaces/interfaces';

const router = Router();

router
  .get('/:id', (req, res, next) => messageService.getMessageById(req.params.id)
    .then((list: any) => res.send(list))
    .catch(next))
  .get('/', (req, res, next) => messageService.getMessages(req.query)
    .then((list: any) => res.send(list))
    .catch(next))
  .post('/reaction', (req, res, next) => messageService.setReaction((<IRequest> req).user.id, req.body)
    .then((result: any) => res.send(result))
    .catch(next))
  .post('/', (req, res, next) => messageService.createMessage((<IRequest> req).user.id, req.body)
    .then((mess: any) => res.send(mess))
    .catch(next))
  .put('/', (req, res, next) => messageService.updateMessage((<IRequest> req).user.id, req.body)
    .then((mess: any) => res.send(mess))
    .catch(next))
  .delete('/:id', (req, res, next) => messageService.deleteMessage(req.params.id)
    .then((result: any) => res.send(result))
    .catch(next));

export default router;
