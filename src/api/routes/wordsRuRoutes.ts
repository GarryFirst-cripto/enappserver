import { Router } from 'express';
import wordRuMiddlware from '../middlewares/wordRuMiddleware';
import * as wordsRuService from '../services/wordsRuServices';
import { IRequest } from '../interfaces/interfaces';

const router = Router();

router
  .get('/:id', (req, res, next) => wordsRuService.getWordById(<IRequest> <unknown> req)
    .then(word => res.send(word))
    .catch(next))
  .get('/', (req, res, next) => wordsRuService.getWords(req.query)
    .then(words => res.send(words))
    .catch(next))
  .post('/complex', (req, res, next) => wordsRuService.getWordsComplex(<IRequest> <unknown> req)
    .then(words => res.send(words))
    .catch(next))
  .post('/', wordRuMiddlware, (req, res, next) => wordsRuService.create(req.body)
    .then(word => res.send(word))
    .catch(next))
  .put('/', (req, res, next) => wordsRuService.updateWord(req.body)
    .then(word => res.send(word))
    .catch(next))
  .delete('/:id', (req, res, next) => wordsRuService.deleteWord(req.params.id)
    .then(item => res.send(item))
    .catch(next));

export default router;
