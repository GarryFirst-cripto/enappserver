import { Router } from 'express';
import * as hintsService from '../services/hintsService';

const router = Router();

router
  .get('/', (req, res, next) => hintsService.getHints(req.query)
    .then(hint => res.send(hint))
    .catch(next))
  .post('/list', (req, res, next) => hintsService.getHintsList(req.body)
    .then(hints => res.send(hints))
    .catch(next))
  .post('/', (req, res, next) => hintsService.create(req.body)
    .then(hint => res.send(hint))
    .catch(next))
  .put('/', (req, res, next) => hintsService.update(req.body)
    .then((hint) => res.send(hint))
    .catch(next))
  .delete('/wordhints/:wordRuId', (req, res, next) => hintsService.deleteWordHints(req.params.wordRuId)
    .then(item => res.send(item))
    .catch(next))
  .delete('/:id', (req, res, next) => hintsService.deleteHint(req.params.id)
    .then(item => res.send(item))
    .catch(next));

export default router;
