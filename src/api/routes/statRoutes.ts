import { Router } from 'express';
import { IRequest } from '../interfaces/interfaces';
import * as statService from '../services/statService';

const router = Router();

router
  .get('/', (req, res, next) => statService.getStat(req.query)
    .then(logs => res.send(logs))
    .catch(next))
  .post('/list', (req, res, next) => statService.getStatList(<IRequest>req)
    .then(items => res.send(items))
    .catch(next))
  .post('/', (req, res, next) => statService.create(<IRequest>req)
    .then(item => res.send(item))
    .catch(next))
  .put('/', (req, res, next) => statService.update(req.body)
    .then((item) => res.send(item))
    .catch(next))
  .delete('/userstat/:userId', (req, res, next) => statService.deleteUserStat(req.params.userId)
    .then(item => res.send(item))
    .catch(next))
  .delete('/:id', (req, res, next) => statService.deleteStat(req.params.id)
    .then(item => res.send(item))
    .catch(next));

export default router;
