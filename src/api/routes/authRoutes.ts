import { Router } from 'express';
import * as authService from '../services/authService';

const router = Router();

router
  .get('/google', (req, res, next) => authService.googleAuth(req, res)
    .catch(next))
  .get('/google/login', (req, res, next) => authService.googleLogin(req, res)
    .catch(next))
  .get('/google/register', (req, res, next) => authService.googleRegister(req, res)
    .catch(next))
  .get('/facebook', (req, res, next) => authService.facebookAuth(req, res)
    .catch(next))
  .get('/facebook/login', (req, res, next) => authService.facebookLogin(req, res)
    .catch(next))
  .get('/facebook/register', (req, res, next) => authService.facebookRegister(req, res)
    .catch(next))
  .get('/vkontakte', (req, res, next) => authService.vkontakteAuth(req, res)
    .catch(next))
  .get('/vkontakte/login', (req, res, next) => authService.vkontakteLogin(req, res)
    .catch(next))
  .get('/vkontakte/register', (req, res, next) => authService.vkontakteRegister(req, res)
    .catch(next))

export default router;
