import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/', (req, res, next) => commentService.getComments(req.query)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/list', (req, res, next) => commentService.getCommentsList(req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/', (req, res, next) => commentService.update(req.body)
    .then((comment) => res.send(comment))
    .catch(next))
  .delete('/comments/:wordRuId', (req, res, next) => commentService.deleteWordComments(req.params.wordRuId)
    .then(item => res.send(item))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteComment(req.params.id)
    .then(item => res.send(item))
    .catch(next));

export default router;
