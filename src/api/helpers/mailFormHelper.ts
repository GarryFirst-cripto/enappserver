const docHeader = '<!DOCTYPE HTML> <html lang="ru"> <head> <meta charset="utf-8"/>'
  + ' <title> Message from Chat </title> </head> <body>';

const resetMessage = 'This letter was sent to you because a password change request was received'
  + ' on the server.<br /> If you did not send such a request, just ignore this letter.<br /><br />'
  + 'To change your password use this verification kode : ';

const resetForm = (kode:string) => (
  `<form style="width: 350px; margin-left: 50px;`
  + ' padding: 15px; font-size: 18px; background-color: #94dcc0; border: 1px #002aff solid">'
  + `<input type="text" name ="pass" readonly="readonly" style="background-color: #e7e7f6" value=${kode} >` 
  + '</form>'
);

export default (kode:string) => (
  `${docHeader} <h3> ${resetMessage} </h3> <br /> ${resetForm(kode)}`
);
