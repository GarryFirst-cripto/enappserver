export const parce = (value:string) => {
  const result:any = {};
  const array = value.split('&');
  for (let i = 0; i < array.length; i++) {
    const item = array[i].split('=');
    // eslint-disable-next-line prefer-destructuring
    result[item[0]] = item[1];
  }
  return result;
};