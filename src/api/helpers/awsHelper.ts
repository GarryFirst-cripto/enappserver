import AWS from 'aws-sdk';
import genUniqueId from './uniqueHelper';
import { env } from '../../config/dbConfig';

AWS.config.update({
  region: 'us-west-1',
  accessKeyId: env.aws.accessKeyId,
  secretAccessKey: env.aws.secretAccessKey
});

const { bucketName } = env.aws;
// const bucketName = 'enapp-data-bucket-nodeserver';
let bucketUrl: string;

const getUrl = new AWS.S3().getSignedUrlPromise('getObject', { Bucket: bucketName, Key: 'key' });
getUrl.then( function(url) {
  const i = url.indexOf('key?');
  bucketUrl = url.substr(0, i - 1);
  console.log(`Amazon Url : ${bucketUrl}`);  
});

const testAwsBucket = async () => {
  const list = await new AWS.S3().listBuckets().promise();
  if (list.Buckets.some(item => item.Name === bucketName)) return;
  await new AWS.S3({ apiVersion: '2006-03-01' }).createBucket({ Bucket: bucketName }).promise();
  console.log(`Amazon bucket "${bucketName}" created.`);
}
testAwsBucket();

export async function uploadFile(buffer: any) {
  const storeName = genUniqueId({ length: 12 }) + '.mp3';
  const objectParams = { ACL: "public-read", Bucket: bucketName, Key: storeName, Body: buffer };
  await new AWS.S3().putObject(objectParams).promise();
  const link = new AWS.S3().getSignedUrl('getObject', { Bucket: bucketName, Key: storeName });
  return link.substr(0, link.indexOf('?'));
}

export const deleteFile = async (fileName: string) => {
  if (fileName) {
    const keyName = fileName.substr(fileName.lastIndexOf('/') + 1);
    const objectParams = { Bucket: bucketName, Key: keyName };
    const result = await new AWS.S3().deleteObject(objectParams).promise();
    return result;
  }
  return null;
}

export async function loadFilesList() {
    const params = { Bucket: bucketName };
    const { Contents: list } = await new AWS.S3().listObjects(params).promise();
    const result: string[] = [];
    list.forEach(item => result.push(`${bucketUrl}/${item.Key}`));
    return result;
}
