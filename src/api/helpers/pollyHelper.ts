import AWS from 'aws-sdk';
import { env } from '../../config/dbConfig';

const polly = new AWS.Polly({
  region: 'eu-west-1',
  accessKeyId: env.polly.clientPollyId,
  secretAccessKey: env.polly.clientPollySecret
});

export const voice = async (voice:string, text:string) => {
  const params:AWS.Polly.Types.SynthesizeSpeechInput = { OutputFormat: 'mp3', VoiceId: voice, Text: text }
  const gets = new Promise((resolve, reject) => {
    polly.synthesizeSpeech(<AWS.Polly.Types.SynthesizeSpeechInput> params, function (err, res: any) {
      if (err) {
        reject(null)
      } else {
        const data = Buffer.from(res.AudioStream);
        resolve(data);
      }
    })
  })
  return await gets;
};
