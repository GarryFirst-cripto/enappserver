import { env } from '../../config/dbConfig';
import { host } from './googleHelper';
import { callApi, buildQueryString } from './apiHelper';
import { getToken, getEmail, setToken } from './tokenHelper';

const { vkontakte: { clientVkId, clientVkSecret }} = env;

export function vkLoginUrl(mode:string) {
  const url = `https://oauth.vk.com/authorize${buildQueryString([
    { client_id: clientVkId },
    { redirect_uri: `${host}/api/auth/vkontakte/${mode}` },
    { response_type: 'code' },
    { scope: ['email'].join('+') },
    { state: '{}' },
  ])}`;
  return url;
};

const getAccessToken = async (code:string, mode:string) => {
  const { email, access_token, user_id } = await callApi(
    'post',
    `https://oauth.vk.com/access_token${buildQueryString([
      { code: code },
      { client_id: clientVkId },
      { client_secret: clientVkSecret },
      { redirect_uri: `${host}/api/auth/vkontakte/${mode}` },
    ])}`
  );
  return {
    email,
    access_token,
    user_id,
  };
};

const getUserInfo = async (accessToken:string) => {
  const data = await callApi(
    'post',
    `https://api.vk.com/method/users.get${buildQueryString([
      { access_token: accessToken },
      { fields: ['screen_name', 'nickname' ].join(',') },
    ])}&v=5.103`
  );
  const { id, first_name, last_name, screen_name, nickname } = data.response[0];
  return {
    id: id,
    name: first_name + ' ' + last_name || nickname || screen_name,
  };
};

const getVkToken = async (code:string, mode:string) => {
  const { access_token, email } = await getAccessToken(code, mode);
  setToken(code, access_token, email);
  return { access_token, email };
}

export const getVkUser = async (code:string, mode:string) => {
  const token = getToken(code);
  const data = token ? { access_token: token, email: getEmail(code) } : await getVkToken(code, mode);
  const { email, access_token } = data;
  const userInfo = await getUserInfo(access_token);
  const { id, name } = userInfo;
  return { name, email, id, mode: Boolean(token) };
};
