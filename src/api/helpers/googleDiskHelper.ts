/* eslint-disable no-console */
import fs from 'fs';
import readline from 'readline';
import { google } from 'googleapis';
import { OAuth2Client } from 'google-auth-library';

const TOKEN_PATH = 'src/credentials/token.json';
const SCOPES = ['https://www.googleapis.com/auth/drive'];

const googleAuth = JSON.parse(fs.readFileSync('src/credentials/credentials.json').toString());

let googleClient: any;

function getAccessToken(oAuth2Client: OAuth2Client, callback: (arg0: any) => void) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', code => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), error => {
        if (error) return console.error(error);
        console.log('Token stored to', TOKEN_PATH);
        return null;
      });
      callback(oAuth2Client);
      return null;
    });
  });
}

interface IauthInst {
  installed: { 
    client_secret: any; 
    client_id: any; 
    redirect_uris: any; 
  };
}
interface Icallback {
  (authClient: any): void; 
  (arg0: OAuth2Client): void; 
  (arg0: any): void;
}

function authorize(credentials:IauthInst , callback: Icallback) {
  // eslint-disable-next-line camelcase
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]
  );
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getAccessToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token.toString()));
    callback(oAuth2Client);
    return null;
  });
}

authorize(googleAuth, (authClient: any) => {
  googleClient = authClient;
  console.log('Google autorization ready ... ');
});

function getGoogleClient() {
  return googleClient;
}

export default getGoogleClient;
