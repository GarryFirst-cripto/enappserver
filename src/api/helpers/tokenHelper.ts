
interface ITokens {
  code: string;
  token: string;
  email?: string;
}

const tokens: ITokens[] = [];

export const getToken = (code: string) => {
  const findToken = tokens.filter(item => item.code === code);
  const result = (findToken.length > 0) ? findToken[0].token : null;
  return result;
}

export const getEmail = (code: string) => {
  const findToken = tokens.filter(item => item.code === code);
  const result = (findToken.length > 0) ? findToken[0].email : null;
  return result;
}

export const setToken = (code: string, token: string, email: string = null) => {
  tokens.push({ code, token, email });
}
