import axios from 'axios';
import queryString from 'querystring';
import { env } from '../../config/dbConfig';
import { host } from './googleHelper';
import { getToken, setToken } from './tokenHelper';

const { facebook: { clientFacebookId, clientFacebookSecret }} = env;

const stringifiedFacebookParams = (mode:string) => queryString.stringify({
  client_id: clientFacebookId,
  redirect_uri: `${host}/api/auth/facebook/${mode}`,
  scope: ['email', 'user_friends'].join(','), // comma seperated string
  response_type: 'code',
  auth_type: 'rerequest',
  display: 'popup',
});
export const facebookLoginUrl = (mode:string) => `https://www.facebook.com/v4.0/dialog/oauth?${stringifiedFacebookParams(mode)}`;

async function getFacebookAccessToken(code:string, mode:string) {
  const { data } = await axios({
    url: 'https://graph.facebook.com/v4.0/oauth/access_token',
    method: 'get',
    params: {
      client_id: clientFacebookId,
      client_secret: clientFacebookSecret,
      redirect_uri: `${host}/api/auth/facebook/${mode}`,
      code,
    },
  });
  console.log('data', data);
  return data.access_token;
};

async function getFacebookUserInfo(access_token:string) {
  const { data } = await axios({
    url: 'https://graph.facebook.com/me',
    method: 'get',
    params: {
      fields: ['id', 'email', 'name', 'first_name', 'last_name'].join(','),
      access_token: access_token,
    },
  });
  return data;
};

const getFacebookToken = async (code:string, mode:string) => {
  const token = await getFacebookAccessToken(code, mode);
  setToken(code, token);
  return token;
}

export const getFacebookUser = async (code:string, mode:string) => {
  console.log('1', code);
  const tokk = getToken(code);
  console.log('2', tokk);
  const token = tokk || await getFacebookToken(code, mode);
  console.log('3', token);
  const user = await getFacebookUserInfo(<string> token);
  user.mode = Boolean(tokk);
  console.log(user);
  return user;
};
