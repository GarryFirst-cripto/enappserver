import axios from 'axios';
import { env } from '../../config/dbConfig';
import { getToken, setToken } from './tokenHelper';

const querystring = require('querystring');
const { google: { clientGoogleId, clientGoogleSecret }} = env;

export let host:string = '';
export const setHost = (value:string) => {
  host = value.includes('localhost') ? `http://${value}` : `https://${value}`;
};

const stringifiedGoogleParams = (mode:string) => querystring.stringify({
    client_id: clientGoogleId,
    redirect_uri: `${host}/api/auth/google/${mode}`,
    scope: [
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile',
    ].join(' '), // space seperated string
    response_type: 'code',
    access_type: 'offline',
    prompt: 'consent'
});

export const googleLoginUrl = (mode:string) => `https://accounts.google.com/o/oauth2/v2/auth?${stringifiedGoogleParams(mode)}`;

async function getGoogleAccessToken(code:string, mode:string) {
  const { data } = await axios({
    url: `https://oauth2.googleapis.com/token`,
    method: 'post',
    data: {
      client_id: clientGoogleId,
      client_secret: clientGoogleSecret,
      redirect_uri: `${host}/api/auth/google/${mode}`,
      grant_type: 'authorization_code',
      code
    },
  });
  return data.access_token;
};

async function getGoogleUserInfo(access_token:string) {
  const { data } = await axios({
    url: 'https://www.googleapis.com/oauth2/v2/userinfo',
    method: 'get',
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
  return data;
};

const getGoogleToken = async (code:string, mode:string) => {
  const token = await getGoogleAccessToken(code, mode);
  setToken(code, token);
  return token;
}

export const getGoogleUser = async (code:string, mode:string) => {
  const tokk = getToken(code);
  const token = tokk || await getGoogleToken(code, mode);
  const user = await getGoogleUserInfo(<string> token);
  user.mode = Boolean(tokk);
  return user;
};
