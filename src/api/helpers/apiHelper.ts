import axios from 'axios';

export const callApi = async ( method:string, url:string, payload:object = null) => {
  try {
    const payloadString = payload !== null ? JSON.stringify(payload) : null;
    const rc:any = { url, headers: { Accept: 'application/json' }, method };
    if (payloadString) {
      rc.data = payloadString;
      rc.headers['Content-Type'] = 'application/json; charset=UTF-8';
    }
    const result = await axios(rc).then(
      (r) => ({ data: r.data, status: r.status, error: null }),
      (e) => ({ data: null, status: e.response.status, error: e.response.data.error })
    );
    if (!result) {
      return {};
    }
    if (result.status === 400) {
      const errMessage = result.error.message;
      if (errMessage) {
        console.error(errMessage);
        return {};
      }
      return {};
    }
    return result.data;
  } catch (error) {
    console.error('fetch api error', error);
    return {};
  }
};

export const buildQueryString = (items: any[]) => {
  const joined = items
    .map((it) => {
      const key = Object.keys(it)[0];
      return `${key}=${encodeURI(it[key])}`;
    })
    .join('&');
  return joined.length > 0 ? '?' + joined : '';
}
