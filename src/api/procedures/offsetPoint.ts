import { QueryTypes } from "sequelize";
import sequelize from '../../data/db/connection';
import wordsRuRepository from "../../repositories/wordsRuRepository";
import logsRepository from "../../repositories/logsRepository";
import { IDiapazon, ILog } from '../interfaces/interfaces'; 
import { size, step ,depth } from '../services/dataService';

const diapazon:IDiapazon = {
  count: 0,
  bottom: 0,
  bottomWeight: 0,
  top: 0,
  topWeight: 0
}

const getNewWordsCount = async (userId:string) => {
  const query = 'SELECT COUNT(0) FROM "wordRus" LEFT JOIN "wordEns" ON "wordEns"."id" = "wordRus"."wordEnId" LEFT JOIN  "loggs" ON "loggs"."wordEnId"'+
                ` = "wordEns"."id" and "loggs"."wordRuId" = "wordRus"."id" and "loggs"."userId" = '${userId}' WHERE "loggs"."id" is null`; 
  try {
    const data = await sequelize.query(query, { type: QueryTypes.SELECT });
    return data[0];
  } catch (err) {
    return err
  }             
}

const findPoint = async (userId:string, point:number) => {
  do {
    const tryPoint = Math.round((diapazon.top + diapazon.bottom) / 2);
    const dataPoint = await wordsRuRepository.getDataComplex(userId, { from: tryPoint, count: 1 });
    const { wordEn: { dataValues: { weightField } } } = dataPoint[0];
    const weight = parseFloat(weightField);
    if (weight === point) {
      return tryPoint;
    }
    weight > point ? diapazon.bottom = tryPoint : diapazon.top = tryPoint;
    weight > point ? diapazon.bottomWeight = weight : diapazon.topWeight = weight;
  } while (diapazon.top - diapazon.bottom > 1);
  const result = (diapazon.bottomWeight - point) > (point - diapazon.topWeight) ? diapazon.top : diapazon.bottom;
  return result;
}

const getDiapazon = async (userId:string) => {
  const { count } = await getNewWordsCount(userId);
  diapazon.count = parseInt(count);
  diapazon.top = parseInt(count);
  const xx = await wordsRuRepository.getDataComplex(userId, { });
  const first = await wordsRuRepository.getDataComplex(userId, { from: 0, count: 1 });
  const last = await wordsRuRepository.getDataComplex(userId, { from: count - 1, count: 1 });
  if (first[0] && last[0]) {
    const { wordEn: { dataValues: { weightField: minW } } } = first[0];
    const { wordEn: { dataValues: { weightField: maxW } } } = last[0];
    diapazon.bottomWeight = parseFloat(minW);
    diapazon.topWeight = parseFloat(maxW);
  } else {
  }
}

const getUserPoint = async (logg:ILog):Promise<number> => {
  const { wordRuId } = logg;
  const lastWord = await wordsRuRepository.getSingleComplex(wordRuId);
  if (lastWord) {
    const { wordEn: { dataValues: { weightField } } } = lastWord;
    return parseInt(weightField);
  };
  return 0;
}

export const getUserOffset = async (userId:string, mode:string):Promise<number> => {
  await getDiapazon(userId);
  const logs = await logsRepository.lastUserLog(userId);
  const point = logs ? await getUserPoint(logs[0]) :  Math.round((diapazon.bottomWeight + diapazon.topWeight) / 2);
  let offset:number = await findPoint(userId, point);
  if (mode === 'packet') {
    if (logs) {
      offset = logs[0].answer === 'знаю' ? offset + step : offset - step;
    }
    offset = Math.max(offset - Math.round((size * step) / 2 ), 0);
    if ((diapazon.count - offset) < (size * step + depth)) {
      offset = diapazon.count - size * step - depth;
    }
  }
  return offset;
}