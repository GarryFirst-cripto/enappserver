import wordsRepository from '../../repositories/wordsRepository';

export default async (req: any, res: any, next: () => void) => {
  const { wordEnId } = req.body;
  if (wordEnId) {
    const word = await wordsRepository.getById(wordEnId);
    if (!word) {
      return res.status(401).send({ status: 404, message: '"wordEnId" value incorrect - no such english word !' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No field "wordEnId" in data packet !' });
  }
  next();
};
