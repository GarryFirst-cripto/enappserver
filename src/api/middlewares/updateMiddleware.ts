import { Request, Response } from 'express';
import { IRequest } from '../interfaces/interfaces';
import userRepository from '../../repositories/usersRepository';

export default async (req: Request, res: Response, next: () => void) => {
  const { user: { id } } = <IRequest> req;
  const idd = req.body.id || id;
  const { email, sex } = req.body;
  if (email) {
    const mail = email.toLowerCase();
    const userByEmail = await userRepository.getByEmail(mail);
    if (userByEmail && userByEmail.id !== idd) {
      return res.status(403).send({ status: 403, message: 'This EMail is already taken.' });
    }
  };
  if (sex && !['male', 'female'].includes(sex)) {
    return res.status(404).send({ status: 404, message: 'User <sex> will by <male> of <female> !' });
  }
  next();
};