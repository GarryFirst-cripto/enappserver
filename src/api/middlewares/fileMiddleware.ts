import multer from 'multer';
import { env } from '../../config/dbConfig';

const storage = multer.memoryStorage();
const upload = multer({
  storage,
  limits: {
    fileSize: parseInt(env.filesize)
  }
});

export default upload.single('audio');
