export default (_req: any, res: any, next: (arg0: { status: number; message: string; }) => void) => {
  if (res.headersSent) {
    next({ status: 500, message: "" });
  } else {
    res.status(500).send({ status:500, message: "" });
  }
};
