import jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';
import { env } from '../../config/dbConfig';
import { IRequest, IToken } from '../interfaces/interfaces';

export default (routesWhiteList: any[]) => (req: Request, res: Response, next: () => any) => (
  routesWhiteList.some(route => req.path.startsWith(route))
    ? next()
    : jwtMiddleware(<IRequest> req, res, next) // auth the user if requested path isn't from the white list
);

async function jwtMiddleware(req: IRequest, res: Response, next: () => any) {
  const headers = JSON.parse(JSON.stringify(req.headers));
  const { 'authorization': token } = headers;
  if (token) {
    try {
      const { id } = <IToken> jwt.verify(token, env.crypto_key);
      const user = await userRepository.getById(id);
      const { sex } = user;
      if (user) {
        req.user = { id, sex };
        next();
      } else {
        res.status(401).send({ status: 401, message: 'Unauthorized.' });
      }
    } catch (e) {
      res.status(401).send({ status: 401, message: 'Unauthorized.' });
    }
  } else {
    res.status(401).send({ status: 401, message: 'Unauthorized.' });
  };
};
