import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { password, email, sex } = req.body;
  if (email) {
    const mail = email.toLowerCase();
    const userByEmail = await userRepository.getByEmail(mail);
    if (userByEmail) {
      return res.status(403).send({ status: 401, message: 'Этот почтовый адрес уже используется.' });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'Необходимо указать корректный почтовый адрес !' });
  }
  if (!password) {
    return res.status(403).send({ status: 401, message: 'Необходимо указать пароль !' });
  }
  if (!['male', 'female'].includes(sex)) {
    // return res.status(404).send({ status: 404, message: 'User <sex> will by <male> of <female> !' });
    req.body.sex = 'default';
  }
  return next();
};