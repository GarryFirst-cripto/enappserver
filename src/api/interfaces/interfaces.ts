import { Request } from 'express';

export interface IRequest extends Request {
  user: { 
    id: string;
    sex: string;
  }
}

export interface IEmailReset {
  email: string,
  kode?: string,
  repeat?: boolean
}

export interface IToken {
  id: string;
}

export interface IListFilter {
  from: number;
  count: number;
  required?: boolean;
  translations?: number;
  group: string;
  status?: string;
  answer?: string;
}

export interface IComplexFilter {
  minWeight?: number,
  maxWeight?: number,
  from?: number,
  count?: number
}

export interface IUser {
  id: string;
  sex: string;
  age: Date;
  pushId: string;
  status: string;
  sensay: string;
  verify: string;
}

export interface IWord {
  id: string;
  ID: string;
  Core: string; 
  WordEn: string;
  WordEnFrq: number; 
  Transcription: string; 
  Meaning: string;
  MeaningProc: number;
  WordRu: string;
  RuComment: string;
  RuUsage: string;
  Hint1: string;
  Hint2: string;
  Hint3: string;
  HintRtg: number;
  groupTagList: string;
  spPart: string;
  partFrequency: number;
}

export interface ILog {
  id: string;
  userId: string;
  wordEnId: string;
  wordRuId: string;
  last: Date;
  lastPlane: Date;
  lastRepeat: Date;
  answer: string;
  status: string;
  plane: Date;
}

export interface IComment {
  id: string;
  wordEnId: string;
  wordRuId: string;
  Comment: string;
  Index: number;
}

export interface IHint {
  id: string;
  wordEnId: string;
  wordRuId: string;
  Hint: string;
  Index: number;
}

export interface IGroupTag {
  id: string;
  groupName: string;
  Info: string,
  Index: number,
}

export interface IStat {
  id: string;
  userId: string;
  experience: number;
  intensity: number;
  status: string;
}

export interface ILinks {
  fileId?: string;
  link?: string;
}

export interface IDiapazon {
  count: number;
  bottom: number;
  bottomWeight: number;
  top: number;
  topWeight: number;  
}
