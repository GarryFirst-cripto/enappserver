import jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';
import logsRepository from '../../repositories/logsRepository';
import { env } from '../../config/dbConfig';
import { IEmailReset } from '../interfaces/interfaces';
import sendEMail from '../helpers/emailHelper';
import { IListFilter } from '../interfaces/interfaces';

export const getUsers = async (filter: IListFilter) => await userRepository.getList(filter);
export const getUserById = async (userId: string) => await userRepository.getById(userId);
export const deleteUser = async (id: string) =>  {
  const { result: logs } = await logsRepository.deleteUserLogs(id);
  const { result } = await userRepository.delete(id);
  return { result, logs };
}

export const updateUser = async (userId: string, updUser: any) => {
  const id = updUser.id || userId;
  return await userRepository.updateById(id, updUser);
}

export const login = async ( req: Request, res: Response ) => {
  const { email: mail, password } = req.body;
  if (mail) {
    const email = mail.toLowerCase();
    const user = await userRepository.getByEmail(email);
    if (user && user.password === password) {
      const { id } = user;
      const token = jwt.sign({ id }, env.crypto_key);
      return res.status(200).send({ token, user });
    } else {
      return res.status(404).send({ status: 404, message: 'No such e-mail or password incorrect ...' });
    };
  }
  return res.status(404).send({ status: 404, message: 'No data to login ...' });
}

export const emailReset = async ( data: IEmailReset, res: Response) => {
  const { email: mail, kode, repeat } = data;
  const email = mail.toLowerCase();
  const user = await userRepository.getByEmail(email);
  if (user) {
    if (kode) {
      if (user.verify === kode) {
        const { id } = user;
        const token = jwt.sign({ id }, env.crypto_key);
        await userRepository.updateById(user.id, { verify: '' });
        return res.status(200).send({ token, user });
      } else {
        return res.status(403).send({ status: 403, message: 'Incorrect code. Try again ...' });
      };
    } else {
      //  do send email ....
      const vKode = (repeat && user.verify) ? user.verify : Math.round(99999 + 900000 * Math.random()).toString();
      await userRepository.updateById(user.id, { verify: vKode });
      const result = await sendEMail(email, vKode);
      return res.status(200).send({ status: 200, result });
    }
  } else {
    return res.status(404).send({ status: 404, message: 'No such e-mail ...' });
  };
}

export const register = async (newUser: any) => {
  const user = await userRepository.create(newUser);
  const { id } = user;
  const token = user ?  jwt.sign({ id }, env.crypto_key) : null;
  return { token, user }
}

export interface IPwd {
  pwd: string
}

export const doAdminAuth = async (PWD: IPwd = { pwd: '' }) => {
  const { pwd } = PWD;
  const { adminName, adminPWD } = env.admin;
  if (pwd === adminPWD) {
    let admin = await userRepository.getByEmail(adminName);
    if (!admin) {
      admin = await userRepository.create({ password: adminPWD,  email: adminName, sex: 'default' });
    }
    const { id } = admin;
    const token = jwt.sign({ id }, env.crypto_key);
    return { token, user: admin };
  }
  return { status: 403, message: 'Incorrect admin password given. Try again ...' };
}
