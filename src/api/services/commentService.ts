import commentRepository from '../../repositories/commentRepository';
import { IComment, IListFilter } from '../interfaces/interfaces';

export const create = async (item: IComment) => await commentRepository.create(item);
export const update = async (item: IComment) => await commentRepository.updateById(item.id, item);
export const getComments = async (filter: object) => await commentRepository.getList(filter);
export const getCommentsList = async (filter: IListFilter) => await commentRepository.getList(filter);
export const deleteWordComments = async (wordRuId: string) => await commentRepository.deleteWordComments(wordRuId);
export const deleteComment = async (id: string) => await commentRepository.delete(id);
