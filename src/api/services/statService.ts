import statRepository from '../../repositories/statRepository';
import { IStat, IRequest } from '../interfaces/interfaces';

export const create = async (req: IRequest) => {
  const userId = req.user;
  const body = req.body;
  const result = await statRepository.create({ userId, ...body });
  return result;
};
export const update = async (item: IStat) => await statRepository.updateById(item.id, item);
export const getStat = async (filter: object) => await statRepository.getStat(filter);
export const getStatList = async (req: IRequest) => await statRepository.getList(req.user.id, req.body);
export const deleteUserStat = async (userId: string) => await statRepository.deleteUserStat(userId);
export const deleteStat = async (id: string) => await statRepository.delete(id);
