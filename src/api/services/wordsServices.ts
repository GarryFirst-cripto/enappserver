import wordsRepository from '../../repositories/wordsRepository';
import audioRepository from '../../repositories/audioRepository';
import { WordEnModel, WordRuModel } from '../../repositories/Models';
// import *  as audioService from './audioService';
import { IWord } from '../interfaces/interfaces';
import { IRequest } from '../interfaces/interfaces';
// import { host } from '../helpers/googleHelper';
import { deleteFile, loadFilesList } from '../helpers/awsHelper';

export const create = async (data: any) => await wordsRepository.create(data);
export const getWords = async (filter: any) => await wordsRepository.getList(filter);
export const getWordById = async (req: IRequest) => await wordsRepository.getWordById(req.user.id, req.user.sex, req.params.id);
export const getWordsList = async (req: IRequest) => await wordsRepository.getWordsList(req.user.id, req.user.sex, req.body);
export const getWordsByStatus = async (req:  IRequest) => await wordsRepository.getWordsByStatus(req.user.id, req.body);
export const getNewWords = async (req:  IRequest) => await wordsRepository.getNewWords(req.user.id, req.user.sex, req.body);
export const getWordsComplex = async (req:  IRequest) => await wordsRepository.getWordsFullList(req.user.id, req.user.sex, req.body, req.params.mode);

export const deleteWord = async (id: string) => {
  const word = await wordsRepository.getById(id);
  await deleteFile(word.audio);
  return await wordsRepository.delete(id);
}

export const createWord = async (newWord: IWord | IWord[]) => {
  if (Array.isArray(newWord)) {
    const result:any[] = [];
    for (let i = 0; i < newWord.length; i++) {
      result.push(await wordsRepository.createNewEnWord(newWord[i]));
    }
    return result;
  } else {
    return await wordsRepository.createNewEnWord(newWord);
  }
}

export const updateWord = async (updWord: IWord) => {
  const { id } = updWord;
  if (Object.keys(updWord).includes('audio')) {
    const { audio } = await wordsRepository.getById(id);
  }
  return await wordsRepository.updateById(id, updWord);
}

export const performControl = async () => {
  await WordEnModel.update({ service: 0 }, { where: {} });
  await WordRuModel.update({ service: 0 }, { where: {} });
  console.log(await wordsRepository.removeEmptyWords());
  const fileList = await loadFilesList(); // audioService.loadFilesList();
  const audioList = await audioRepository.getAll();
  const controll = (link: string):boolean => {
    let result = false;
    for (let j = 0; j < fileList.length; j++) {
      if (link === fileList[j]) {
        fileList.splice(j, 1);
        result = true;
        break;
      }
    };
    return result;
  }
  let count = 0;
  for (let i = 0; i < audioList.length; i++) {
    // const k = audioList[i].link.lastIndexOf('/');
    const link = audioList[i].link; // .substr(k+1);
    if (controll(link) === false) {
      await audioRepository.delete(audioList[i].id);
      count ++;
    }
  }
  let files = 0;
  // console.log(`Delete ${fileList.length} files`)
  for (let i = 0; i < fileList.length; i++) {
    const result = await deleteFile(fileList[i]);
    if (result) {
      files ++;
    }
  }
  console.log(`Perform ${count} lines and ${files} files`);
  return { links: count, files };
}
