import groupTagRepository from '../../repositories/groupTagRepository';
import { IGroupTag } from '../interfaces/interfaces';

export const create = async (item: IGroupTag) => await groupTagRepository.create(item);
export const update = async (item: IGroupTag) => await groupTagRepository.updateById(item.id, item);
export const getGroupTags = async () => await groupTagRepository.getAll();
export const deleteGroupTag = async (id: string) => await groupTagRepository.delete(id);
