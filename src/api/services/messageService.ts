import messageRepository from '../../repositories/messageRepository';
import messReactionRepository from '../../repositories/messReactionRepository';
import { IListFilter } from '../interfaces/interfaces';

export const getMessages = async (filter: any) => await messageRepository.getMessages(<IListFilter> filter);
export const getMessageById = async (id: string) => await messageRepository.getMessageById(id);
export const createMessage = async (userId:string, data: any) => await messageRepository.createMessage(userId, data);
export const updateMessage = async (userId:string, data: any) => await messageRepository.updateMessage(userId, data.id, data);

interface IReaction {
  messageId: string; 
  isLike: boolean;
}

interface INewReaction {
  id:string;
  isLike: boolean;
}

export const deleteMessage = async (id:string) => {
  const lines = await messReactionRepository.deleteByMessage(id);
  const result = await messageRepository.delete(id);
  return { result, lines };
}

export const setReaction = async (userId: string, reaction: IReaction) => {
  // define the callback for future use as a promise
  const { messageId, isLike } = reaction;
  const updateOrDelete = (react: INewReaction) => (react.isLike === isLike
    ? messReactionRepository.delete(react.id)
    : messReactionRepository.updateById(react.id, { isLike }));
  const calcDopp = (react: INewReaction) => (react.isLike === isLike ? 0 : 1);

  const oldReaction = await messReactionRepository.getMessReaction(userId, messageId);
  const result = oldReaction
    ? await updateOrDelete({ isLike, id: oldReaction.id })
    : await messReactionRepository.create({ userId, messageId, isLike });
  const dopp = oldReaction ? calcDopp(oldReaction.isLike) : 0;
  // the result is an integer when an entity is deleted
  if (Number.isInteger(result)) return { dopp };
  const id = await messReactionRepository.getMessReaction(userId, messageId);
  return ({ id, dopp });
};
