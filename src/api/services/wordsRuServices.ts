import wordsRuRepository from '../../repositories/wordsRuRepository';
import { IRequest } from '../interfaces/interfaces';

export const getWords = async (filter: any) => await wordsRuRepository.getList(filter);
export const getWordById = async (req: IRequest) => await wordsRuRepository.getWordById(req.user.id, req.params.id);
export const create = async (data: any) => await wordsRuRepository.createRuWord(data);
export const updateWord = async (data: any) => await wordsRuRepository.updateById(data.id, data);
export const deleteWord = async (id: string) =>  await wordsRuRepository.delete(id);
export const getWordsComplex = async (req:  IRequest) => await wordsRuRepository.getWordsComplex(req.user.id, req.user.sex, req.body);
