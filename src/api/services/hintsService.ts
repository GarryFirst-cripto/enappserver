import hintRepository from '../../repositories/hintRepository';
import { IHint, IListFilter } from '../interfaces/interfaces';

export const create = async (item: IHint) => await hintRepository.create(item);
export const update = async (item: IHint) => await hintRepository.updateById(item.id, item);
export const getHints = async (filter: object) => await hintRepository.getList(filter);
export const getHintsList = async (filter: IListFilter) => await hintRepository.getList(filter);
export const deleteWordHints = async (wordRuId: string) => await hintRepository.deleteWordHints(wordRuId);
export const deleteHint = async (id: string) => await hintRepository.delete(id);
