import { drive_v3, google } from 'googleapis';
import { Readable } from 'stream';
import { Request, Response } from 'express';
import fs from 'fs';
import googleClient from '../helpers/googleDiskHelper';
import { host, setHost } from '../helpers/googleHelper';
import { voice } from '../helpers/pollyHelper';
import audioRepository from '../../repositories/audioRepository';
import wordsRepository from '../../repositories/wordsRepository';
import { IListFilter, ILinks } from '../interfaces/interfaces';
import { parce } from '../helpers/arrayHelper';
import { uploadFile } from '../helpers/awsHelper';

export async function loadFile(fileId:string) {
  if (!fileId || fileId === 'undefined') {
    return {};
  }
  const auth = googleClient();
  const drive = google.drive({ version: 'v3', auth });
  const load = new Promise((resolve, reject) => drive.files.get({
    fileId,
    alt: 'media'
  }, { responseType: 'arraybuffer' }, (err, { data }) => {
    if (err) {
      reject(err);
    } else {
      const ress = Buffer.from(<any> data);
      resolve(ress);
    }
  }));
  const result = await load;
  return result;
}

// export async function uploadFile(fileBlob:ArrayBuffer, storeName:string):Promise<ILinks> {
//   if (fileBlob) {
//     const stream = new Readable();
//     stream._read = () => {};
//     stream.push(fileBlob);
//     stream.push(null);
//     const auth = googleClient();
//     const drive = google.drive({ version: 'v3', auth });
//     const fileMetadata = { name: storeName };
//     const media = { mimeType: 'image/jpeg', body: stream };
//     const file = await drive.files.create(<any>  {
//       includePermissionsForView: 'published',
//       resource: fileMetadata,
//       media
//     });
//     const { data } = file;
//     if (data) {
//       const { id: fileId } = data;
//       drive.permissions.create({ fileId, requestBody: { role: 'reader', type: 'anyone' } });
//       const webViewLink = await drive.files.get({ fileId, fields: 'webViewLink' });
//       const link = webViewLink ? webViewLink.data.webViewLink : '';
//       return { fileId, link }
//     }
//   }
//   return {};
// }

export const postAudio = async (req: Request, res: Response) => {
  setHost(req.headers.host);
  const { file: { originalname, buffer } } = req;
  const { wordId, name, language, voice, sex } = parce(originalname);
  const word = await wordsRepository.getById(wordId);
  if (word) {
    const links = await uploadFile(buffer); // , name || 'audio.mp3');
    if (links) {
      // const { fileId, link: publicLink } = links;
      // const fieldId = links;
      const publicLink = links;
      const link = links; // `${host}/audio/${fileId}`;
      const { id: wordId } = word;
      const result = await audioRepository.create({
        wordId,
        language,
        voice,
        sex,
        publicLink,
        link
      });
      return res.status(200).send(result);
    }
    return res.status(500).send({ status: 500, message: 'Error writing new image ...' });
  }
  return res.status(500).send({ status: 500, message: 'No such good ...' });
};

// export async function loadFilesList() {
//   const auth = googleClient();
//   const drive = google.drive({version: 'v3', auth});
//   const result: drive_v3.Schema$File[] = [];
//   let token = '';
//   do {
//     const list = await drive.files.list({
//       q: 'trashed = false',   // get items not in trash
//     // fields: 'files(id, name)'  // set field list
//       pageSize: 1000,
//       pageToken: token
//     });
//     const { data: { files: resultPart, nextPageToken } } = list;
//     token = nextPageToken;
//     result.push( ... resultPart);
//   } while (token);
//   return result;
// }

// export async function deleteFile(fileId:string) {
//   if (fileId && fileId.startsWith(host)) {
//     const i = fileId.lastIndexOf('/');
//     if (i >= 0) {
//       // eslint-disable-next-line no-param-reassign
//       fileId = fileId.substr(i + 1);
//     }
//     const auth = googleClient();
//     const drive = google.drive({ version: 'v3', auth });
//     try {
//       const dell = new Promise((resolve, reject) => drive.files.delete({ fileId }, err => {
//         if (err) {
//           reject(false);
//         } else {
//           resolve(true);
//         }
//       }));
//       const result = await dell;
//       return result;
//     } catch {
//       return false;
//     }
//   }
//   return false;
// }

export const updateVoice = async (updVoice: any) => await audioRepository.updateById(updVoice.id, updVoice);
export const deleteVoice = async (id:string ) =>  await audioRepository.delete(id);

interface IAudio {
  voice: string
}

const getWordVoices = async (word: any) => {
  let result = 0;
  // for (let i = 0; i < voiceList.length; i++ ) {
  for (let i = 0; i < 2; i++ ) {   // *****************************************************************
    if (!word.audios.some((item: IAudio) => item.voice === voiceList[i].voice)) {
      const data = await voice(voiceList[i].voice, word.WordEn);
      if (data) {
        const links = await uploadFile(<ArrayBuffer> data); // , `${voiceList[i].voice}.mp3`);
        if (links) {
          // const { fileId, link: publicLink } = links;
          const publicLink = links;
          const link = links; //`${host}/audio/${fileId}`;
          const { id: wordId } = word;
          await audioRepository.create({
            wordId,
            language: voiceList[i].language,
            voice: voiceList[i].voice,
            sex: voiceList[i].sex,
            publicLink,
            link
          });
          result += 1;
        }
      }
    }
  }
  return result;
}

const voiceList = JSON.parse(fs.readFileSync('src/data/voices.json').toString());
export const loadVoices = async (req: Request, res: Response) => {
  setHost(req.headers.host);
  // await audioRepository.clearAudioList();
  const words = await wordsRepository.getAudioList(<IListFilter> <unknown>req.query);
  let result = 0;
  for (let i = 0; i < words.length; i++ ) {
    console.log(words[i].id);
    result = result + await getWordVoices(words[i]);
  }
  console.log(`Perform : ${result}`)
  res.status(200).send({ result });
}

export const loadVoicesById = async (req: Request, res: Response) => {
  setHost(req.headers.host);
  const word = await wordsRepository.getAudioListById(req.params.id);
  await getWordVoices(word);
  console.log('perform');
  res.status(200).send({ result: 1 });
}
