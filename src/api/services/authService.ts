import { Request, Response } from 'express';
import registerMiddlware from '../middlewares/registrMiddleware';
import userRepository from '../../repositories/usersRepository';
import { login, register } from './usersServices';
import { getGoogleUser, googleLoginUrl, setHost } from '../helpers/googleHelper';
import { getFacebookUser, facebookLoginUrl } from '../helpers/facebookHelper';
import { getVkUser, vkLoginUrl } from '../helpers/vkontakteHelper';

export const googleAuth = async (req:Request, res:Response) => {
  setHost(req.headers.host);
  if (Object.keys(req.query).includes('login')) {
    return res.redirect(googleLoginUrl('login'));
  };
  if (Object.keys(req.query).includes('register')) {
    return res.redirect(googleLoginUrl('register'));
  };
  res.status(500).send({ status: 500, message: 'No command in request ...' });
}

export const googleLogin = async (req:Request, res:Response) => {
  const code = <string>req.query.code;
  const googleUser = await getGoogleUser(code, 'login');
  if (googleUser) {
    const { email: mail, id: password } = googleUser;
    const email = mail.toLowerCase();
    req.body = { email, password };
    await login(req, res); 
    return null;
  }
  return res.status(403).send({ status: 403, message: 'Error get google user ...' });
}

export const googleRegister = async (req:Request, res:Response) => {
  const code = <string>req.query.code;
  const googleUser = await getGoogleUser(code, 'register');
  if (googleUser) {
    const { email, id: password, mode } = googleUser;
    if (mode) {
      req.body = { email, password, sex: 'male' };
      if (await registerMiddlware(req, res, () => {return true}) === true) {
        const result = await register(req.body);
        return res.status(200).send(result);
      }
      return null;
    }
    return res.status(200).send({ result: 'Ok' });
  }
  return res.status(403).send({ status: 403, message: 'Error get google user ...' });
}

export const facebookAuth = async (req:Request, res:Response) => {
  setHost(req.headers.host);
  if (Object.keys(req.query).includes('login')) {
    return res.redirect(facebookLoginUrl('login'));
  };
  if (Object.keys(req.query).includes('register')) {
    return res.redirect(facebookLoginUrl('register'));
  };
  res.status(500).send({ status: 500, message: 'No command in request ...' });
}

export const facebookLogin = async (req:Request, res:Response) => {
  const code = <string>req.query.code;
  const facebookUser = await getFacebookUser(code, 'login');
  if (facebookUser) {
    const { email: mail, id: password } = facebookUser;
    const email = mail.toLowerCase();
    req.body = { email, password };
    await login(req, res); 
    return null;
  }
  return res.status(403).send({ status: 403, message: 'Error get facebook user ...' });
}

export const facebookRegister = async (req:Request, res:Response) => {
  const code = <string>req.query.code;
  const facebookUser = await getFacebookUser(code, 'register');
  if (facebookUser) {
    const { email, id: password, mode } = facebookUser;

    if (mode) {
      req.body = { email, password, sex: 'male' };
      if (await registerMiddlware(req, res, () => {return true}) === true) {
        const result = await register(req.body);
        return res.status(200).send(result);
      }
      return null;
    }
    return res.status(200).send({ result: 'Ok' });

  }
  return res.status(403).send({ status: 403, message: 'Error get facebook user ...' });
}

export const vkontakteAuth = async (req:Request, res:Response) => {
  setHost(req.headers.host);
  if (Object.keys(req.query).includes('login')) {
    return res.redirect(vkLoginUrl('login'));
  };
  if (Object.keys(req.query).includes('register')) {
    return res.redirect(vkLoginUrl('register'));
  };
  res.status(500).send({ status: 500, message: 'No command in request ...' });
}

export const vkontakteLogin = async (req:Request, res:Response) => {
  const code = <string>req.query.code;
  const vkUser = await getVkUser(code, 'login');
  if (vkUser) {
    const { email: mail, id } = vkUser;
    const password = id.toString();
    const email = mail.toLowerCase();
    req.body = { email, password };
    await login(req, res); 
    return null;
  }
  return res.status(403).send({ status: 403, message: 'Error get VKontakte user ...' });
}

export const vkontakteRegister = async (req:Request, res:Response) => {
  const code = <string>req.query.code;
  const vkUser = await getVkUser(code, 'register');
  if (vkUser) {
    const { email, id: password, mode } = vkUser;

    if (mode) {
      req.body = { email, password, sex: 'male' };
      if (await registerMiddlware(req, res, () => {return true}) === true) {
        const result = await register(req.body);
        return res.status(200).send(result);
      }
      return null;
    }
    return res.status(200).send({ result: 'Ok' });

  }
  return res.status(403).send({ status: 403, message: 'Error get VKontakte user ...' });
}
