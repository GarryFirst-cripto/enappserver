import logsRepository from '../../repositories/logsRepository';
import { ILog } from '../interfaces/interfaces';
import { IEmptyData } from  '../../repositories/interfaces/repoInterfaces';

export const getLogs = async (filter: object) => await logsRepository.getList(filter);
export const getFullLogs = async (filter: object) => await logsRepository.getFullList(filter);
export const deleteUserLogs = async (userId: string) => await logsRepository.deleteUserLogs(userId);
export const deleteWordLogs = async (wordId: string) => await logsRepository.deleteWordLogs(wordId);
export const deleteLog = async (id: string) => await logsRepository.delete(id);
export const emptyLogs = async (data: IEmptyData) => await logsRepository.emptyLogs(data);

export const createLog = async (req: any) => {
  const userId = req.user.id;
  const newLog  = req.body; 
  if (Array.isArray(newLog)) {
    const result:ILog[] = [];
    for (let i = 0; i < newLog.length; i++) {
      newLog[i].userId = userId;
      result.push(await logsRepository.create(newLog[i]));
    };
    return result;
  } else {
    newLog.userId = userId;
    return await logsRepository.create(newLog);
  }
}

export const updateLog = async (req: any) => {
  const userId = req.user.id;
  const updLog  = req.body; 
  if (Array.isArray(updLog)) {
    const result:ILog[] = [];
    for (let i = 0; i < updLog.length; i++) {
      const item = updLog[i];
      const { id } = item;
      if (id) {
        delete item.id;
        result.push(await logsRepository.updateById(id, item));
      } else {
        item.userId = item.userId || userId;
        result.push(await logsRepository.create(item));
      }
    }
    return result;
  } else {
    const { id } = updLog;
    delete updLog.id;
    return await logsRepository.updateById(id, updLog);
  }
}
