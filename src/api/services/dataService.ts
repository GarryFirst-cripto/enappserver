import { QueryTypes } from "sequelize";
import sequelize from '../../data/db/connection';
import wordsRepository from "../../repositories/wordsRepository";
import wordsRuRepository from "../../repositories/wordsRuRepository";
import logsRepository from "../../repositories/logsRepository";
import { getUserOffset } from '../procedures/offsetPoint';
import { trainingPlane } from '../../config/trainingPlane';
import { IRequest } from '../interfaces/interfaces';

interface IUser {
  id: string;
  sex: string;
}

export const getDataLength = async (table:object) => {
  const tableName = Object.keys(table)[0];
  const query = `SELECT count(0) FROM ${tableName}`;
  try {
    const data = await sequelize.query(query, { type: QueryTypes.SELECT });
    return data[0];
  } catch (err) {
    return err
  }
}

export const step: number = 10;
export const size: number = 11;
export const depth: number = 5; // *** depth < step !
export const translations: number = 2;

// const createPacket = async (userId:string, userSex:string, offset:number) => {
//   const count = step * size + depth;
//   const list = await wordsRuRepository.getDataComplex(userId, { from: offset, count });
//   const result = [];
//   const idArray = [];
//   for (let i = 0; i < size; i++) {
//     const column: any[] = [];
//     for (let j = 0; j < depth; j++) {
//       let index = i * step + j;
//       const { wordEn: { dataValues: { id } } } = list[index];
//       const { id: wordRuId } = list[index];
//       const word = await wordsRepository.getDataById(userId, userSex, id, wordRuId);
//       idArray.push(word.id)
//       column.push(word);
//     }
//     result.push(column);
//   }
//   let cau = size * depth * translations;
//   const wordsRu = await wordsRuRepository.getTranslations(cau, idArray);
//   cau = 0; 
//   result.forEach(item => {
//     item.forEach(subitem => {
//       subitem.dataValues.translations = [];
//       for (let i = 0; i < translations; i++) {
//         subitem.dataValues.translations.push(wordsRu[cau].WordRu);
//         cau++;
//         if (cau >= wordsRu.length) cau = 0;
//       }
//       const j = Math.round(3 * Math.random() - 0.49999);
//       const word = subitem.wordRus[0].WordRu;
//       subitem.dataValues.translations.splice(j, 0, word);
//     })
//   });
//   return result;
// }

// export const getDataPacket = async (user: IUser) => {
//   const offset = await getUserOffset(user.id, 'packet');
//   const result = await createPacket(user.id, user.sex, offset);
//   return result;
// }

const createList = async (userId:string, userSex:string, offset:number, size: number) => {
  const list = await wordsRuRepository.getDataComplex(userId, { from: offset, count: size });
  const idArray: string[] = [];
  list.forEach((item: any) => {
    const { wordEn: { dataValues: { id } } } = item;
    idArray.push(id);
  });
  const result = wordsRepository.getWordList(userId, userSex, idArray);
  return result;
}

export const getDataList = async (user: IUser, count:string = '40') => {
  const size = parseInt(count);
  let offset = await getUserOffset(user.id, '');
  offset = Math.max(0, Math.round(offset - size / 2));
  const result = await createList(user.id, user.sex, offset, size);
  return result;
}

export const getCommonList = async (userId: string) => {
  const list = await wordsRuRepository.getDataCommon(userId);
  const wordsRu = await wordsRuRepository.getTranslations(null, []);
  let cau = 0; 
  list.forEach((item: any) => {
    item.dataValues.translations = [];
    let text = '';
    for (let i = 0; i < translations; i++) {
      do {
        cau ++;
        if (cau >= wordsRu.length) cau = 0;
      } while (item.WordRu === wordsRu[cau].WordRu || text === wordsRu[cau].WordRu);
      item.dataValues.translations.push(wordsRu[cau].WordRu);
      text = wordsRu[cau].WordRu;
    }
    const j = Math.round(3 * Math.random() - 0.49999);
    const word = item.WordRu;
    item.dataValues.translations.splice(j, 0, word);
  });
  return list;
}

export const getVocabulary = async (user: IUser) => {
  const { id, sex } = user;
  const data = await wordsRepository.getVocabulary(id, sex);
  const list = [];
  for (let i = 0; i < data.length; i ++) {
    const { id, Core, WordEn, WordEnFrq, status, groupTagList, createdAt, audios } = data[i];
    const wordRus: any[] = [];
    for (let j = 0; j < data[i].dataValues.wordRus.length; j ++) {
      const item = { id, Core, WordEn, WordEnFrq, status, groupTagList, createdAt, wordRus, audios };
      const loggs = data[i].dataValues.wordRus[j].loggs.sort((itemA: any, itemB: any) => itemB.index - itemA.index);
      if (loggs[0].state < 16) {
        data[i].dataValues.wordRus[j].loggs = loggs;
        item.wordRus = [data[i].dataValues.wordRus[j]];
        list.push(item);
      }
    }
  }
  return list;
}

const testLogg = (item:any[]):boolean => {
  return item.length > 0 && item[0].state < 16 && item[0].mode === true;
}
const testKnow = (item:any[]):boolean => {
  return item.length > 0 && (item[0].state === 16 || (item[0].mode === false && item[0].answer === true));
}

export const getStatCount = async (req: IRequest) => {
  const { user: { id }} = req;
  let list = await wordsRepository.getShortVocabulary(id);
  for (let j = 0; j < list.length; j ++) {
    for (let i = 0; i < list[j].dataValues.wordRus.length; i++) {
      const loggs = list[j].dataValues.wordRus[i].loggs.sort((itemA: any, itemB: any) => {
        const result = itemB.index - itemA.index;
        if (result === 0) return itemB.mode - itemA.mode;
        return result;
      });
      list[j].dataValues.wordRus[i].loggs = loggs;
    }
  }
  let newWords = list.filter((item: any) => {
    for (let i = 0; i < item.wordRus.length; i++){
      if (item.wordRus[i].loggs.length > 0) return false;
    };
    return true;
  }).length;
  let toLearn = list.filter((item: any) => {
    for (let i = 0; i < item.wordRus.length; i++){
      if (testLogg(item.wordRus[i].loggs)) return true;
    };
    return false;
  }).length;
  const learned = list.filter((item: any) => {
    let result = false;
    for (let i = 0; i < item.wordRus.length; i++){
      if (testLogg(item.wordRus[i].loggs)) return false;
      if (testKnow(item.wordRus[i].loggs)) result = true;
    };
    return result;
  }).length;
  return { newWords, learned, toLearn }
}

export const resetUserLogs = async (user:IUser) => {
  const result = await logsRepository.deleteUserLogs(user.id);
  return result;
}

export const getStatistic = async (req: IRequest) => {
  function setWordStatus(wordEnId: string, wordRuId: string, state: number, wordzone: number, answer: boolean, lastData: Date) {
    for (let i = 0; i < wordStat.length; i++) {
      if (wordStat[i].wordEnId === wordEnId && wordStat[i].wordRuId === wordRuId) {
        wordStat[i].lastData = lastData;
        wordStat[i].count ++;
        if (answer) wordStat[i].countTrue ++;
        wordStat[i].state = state;
        wordStat[i].wordzone = wordzone;
        return;
      }
    }
  }
  function newWordLearned(wordEnId: string, wordRuId: string): number {
    for (let i = 0; i < words.length; i++) {
      if (words[i].wordEnId === wordEnId && words[i].wordRuId === wordRuId) {
        return 0;
      }
    }
    words.push({wordEnId, wordRuId});
    return 1;
  }
  async function getProgress() {
    const now = (new Date()).getTime() / 60000;
    for (let i = 0; i < wordStat.length; i++) {
      const { wordEnId, wordRuId } = wordStat[i];
      const word = await wordsRepository.getWordWord(wordEnId, wordRuId);
      wordStat[i].WordEn = word.WordEn;
      wordStat[i].WordRu = word.wordRus[0].WordRu;
      wordStat[i].waight = word.WordEnFrq * word.wordRus[0].MeaningProc / 100;
      wordStat[i].progress = ((trainingPlane[wordStat[i].state].summtime / (now - wordStat[i].created)) * 100).toFixed(2);
    }
    wordStat.sort((itemA, itemB) => {
      if(itemA.WordEn < itemB.WordEn) { return -1; }
      if(itemA.WordEn > itemB.WordEn) { return 1; }
      return 0;
    })
  }
  const userId = req.query.userId || req.user.id;
  const list = await logsRepository.getList({ userId });
  // const wordStates = {
  //   bronse: { week: 0, month: 0, total: 0 },
  //   silver: { week: 0, month: 0, total: 0 },
  //   gold: { week: 0, month: 0, total: 0 },
  //   learned: { week: 0, month: 0, total: 0 }
  // };
  if (list.length === 0) {
    const stat: any[] = [];
    const statWord: any[] = [];
    return { stat, statWord }; // , wordStates }
  }
  list.forEach((item: any) => {
    item.updatedAt = item.createdAt;
    item.createdAt.setHours(12, 0, 0, 0);
  });
  const loggs = list.sort((itemA: any, itemB: any) => {
    const time = itemA.createdAt.getTime() - itemB.createdAt.getTime();
    if (time === 0) return (itemA.index - itemB.index);
    return time;
  });
  const current = (new Date()).setHours(12, 0, 0, 0);
  let dats = loggs[0].createdAt;
  let allWords = 0;
  let allExp = 0;
  let learned = 0;
  let know = 0;
  const words: any[] = [];
  const stat: any[] = [];
  const wordStat: any[] = [];
  let statObj = { data: dats.toString(), count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
  loggs.forEach((item:any) => {
    if (item.createdAt.getTime() !== dats.getTime()) {
      statObj.allWords = allWords;
      statObj.experience = allExp;
      statObj.learned = learned;
      statObj.know = know;
      stat.push(statObj);
      while (true) {
        dats.setMinutes(dats.getMinutes() + 1440);
        learned = 0;
        words.length = 0;
        statObj = { data: dats.toString(), count: 0, countTrue: 0, allWords: allWords, learned: 0, experience: allExp, know: know };
        if (dats >= item.createdAt) break;
        stat.push(statObj);
      }
    }
    if (item.mode === true) {
      if (item.index > 0) {
        if (item.answer === true) {
          learned = learned + newWordLearned(item.wordEnId, item.wordRuId);
          if (item.state >= 16) {
            know ++;
          }
        };
        setWordStatus(item.wordEnId, item.wordRuId, item.state, item.wordzone, item.answer, item.createdAt);
        statObj.count ++;
        if (item.answer) statObj.countTrue ++;
        allExp = allExp + item.experience;
      }
    }
    // if (item.mode === false && item.answer === true) {
    //   know ++;
    // }
    if (item.mode === false && item.answer === false) {
      const { wordEnId, wordRuId } = item;
      const created = item.updatedAt.getTime() / 60000;
      wordStat.push({ created, wordEnId, wordRuId, state: 0, wordzone: 0, progress: 0, count: 0, countTrue: 0 });
      allWords ++;
    }
  });
  statObj.allWords = allWords;
  statObj.experience = allExp;
  statObj.learned = learned;
  statObj.know = know;
  stat.push(statObj);
  const firstDats = new Date(dats);
  while (dats < current) {
    dats.setMinutes(dats.getMinutes() + 1440);
    statObj = { data: dats.toString(), count: 0, countTrue: 0, allWords: allWords, learned: 0, experience: allExp, know: know };
    stat.push(statObj);
  }
  for (let i = stat.length; i <= 30; i++) {
    firstDats.setMinutes(firstDats.getMinutes() - 1440);
    statObj = { data: firstDats.toString(), count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
    stat.splice(0, 0, statObj);
  }

  await getProgress();

  const stDataZero = new Date();
  stDataZero.setHours(12, 0, 0, 0);
  const dataWeek = new Date(stDataZero);
  dataWeek.setDate(dataWeek.getDate() - 6);
  const weekTiming = dataWeek.getTime();
  const dataMonth = new Date(stDataZero);
  dataMonth.setMonth(dataMonth.getMonth() - 1);
  const monthTiming = dataMonth.getTime();
  // wordStat.forEach(word => {
  //   if (word.lastData) {
  //     const timing = word.lastData.getTime();
  //     switch(word.wordzone) {
  //       case 1 :
  //         if (timing > weekTiming) wordStates.bronse.week ++;
  //         if (timing > monthTiming) wordStates.bronse.month ++;
  //         wordStates.bronse.total ++;
  //         break;
  //       case 2 :
  //         if (timing > weekTiming) wordStates.silver.week ++;
  //         if (timing > monthTiming) wordStates.silver.month ++;
  //         wordStates.silver.total ++;
  //         break;
  //       case 3 :
  //         if (word.state < 16) {
  //           if (timing > weekTiming) wordStates.gold.week ++;
  //           if (timing > monthTiming) wordStates.gold.month ++;
  //           wordStates.gold.total ++;
  //         } else {
  //           if (timing > weekTiming) wordStates.learned.week ++;
  //           if (timing > monthTiming) wordStates.learned.month ++;
  //           wordStates.learned.total ++;
  //         }
  //     }
  //   }
  // });

  const stData = new Date(stDataZero);
  let endData = new Date(stDataZero);
  const timing = new Date(stat[0].data).getTime();
  while (stData.getTime() > timing) {
    endData = new Date(stData);
    stData.setDate(stData.getDate() - 7);
  }
  const statweek: any[] = [];
  statObj = { data: endData.toString(), count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
  let j = 0;
  while (j < stat.length) {
    if (new Date(stat[j].data).getTime() > endData.getTime()) {
      statweek.push(statObj);
      endData.setDate(endData.getDate() + 7);
      statObj = { data: endData.toString(), count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
    }
    statObj.count += stat[j].count;
    statObj.countTrue += stat[j].countTrue;
    statObj.learned += stat[j].learned;
    statObj.allWords = stat[j].allWords;
    statObj.experience = stat[j].experience;
    statObj.know = stat[j].know;
    j ++;
  }
  statweek.push(statObj);
  if (statweek.length <= 1) {
    statObj = { data: '', count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
    statweek.splice(0, 0, statObj);
  }

  const stDataM = new Date(stDataZero);
  while (stDataM.getTime() > timing) {
    endData = new Date(stDataM);
    stDataM.setMonth(stDataM.getMonth() - 1);
  }  
  const statmonth: any[] = [];
  statObj = { data: endData.toString(), count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
  j = 0;
  while (j < stat.length) {
    if (new Date(stat[j].data).getTime() > endData.getTime()) {
      statmonth.push(statObj);
      endData.setMonth(endData.getMonth() + 1);
      statObj = { data: endData.toString(), count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
    }
    statObj.count += stat[j].count;
    statObj.countTrue += stat[j].countTrue;
    statObj.learned += stat[j].learned;
    statObj.allWords = stat[j].allWords;
    statObj.experience = stat[j].experience;
    statObj.know = stat[j].know; 
    j ++;
  }
  statmonth.push(statObj);
  if (statmonth.length <= 1) {
    statObj = { data: '', count: 0, countTrue: 0, allWords: 0, learned: 0, experience: 0, know: 0 };
    statmonth.splice(0, 0, statObj);
  }

  return { stat, statweek, statmonth, wordStat }; // , wordStates };
};
