
const logsSeed = [
  {
    id: 'f86246fd-af27-45d1-aca7-24962ee84cbe',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: 'd2cf28f5-7561-47ab-8474-8d360ac04bd3',
    wordRuId: 'f1861490-a1d7-43c2-a51f-6228160a206d',
    answer: 'жена',
    status: 'Золотой',
    plane: '2021-07-10T21:05:20.344Z'
  },
  {
    id: 'e7939bc3-63c8-427e-901b-4ed4533037d8',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: '1b522a6b-57e2-4a70-a079-e76f951de2a1',
    wordRuId: '37edf198-4b0a-4a7f-a788-9818d34fd1f0',
    answer: 'я',
    status: 'Бронзовый',
    plane: '2021-07-13T21:05:20.344Z'
  },
  {
    id: 'f86246fd-af27-45d1-aca7-24963ee84cbe',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: 'e4da70cb-32d8-4fee-a693-5f250650b72c',
    answer: 'это',
    status: 'Золотой',
    plane: '2021-07-10T21:05:20.344Z'
  },
  {
    id: 'e7939bc3-63c8-427e-901b-4ed4543037d8',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '092dae68-165f-4774-9a6c-fed47a781224',
    answer: 'конечно',
    status: 'Бронзовый',
    plane: '2021-07-13T21:05:20.344Z'
  },
  {
    id: 'e7939bc3-63c8-428e-901b-4ed4543037d8',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '092dae68-165f-4774-9a6c-fed47a781224',
    answer: 'конечно',
    status: 'Серебрянный',
    plane: '2021-07-13T21:05:20.344Z'
  },
  {
    id: 'f86246fd-af37-45d1-aca7-24963ee84cbe',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '092dae68-165f-4774-9a6c-fed47a781224',
    answer: 'это',
    status: 'Серебрянный',
    plane: '2021-07-10T21:05:20.344Z'
  },
  {
    id: 'e7939bc3-64c8-427e-901b-4ed4543037d8',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '3160441d-e7b9-43db-a0a1-203f094a4368',
    answer: 'конечно',
    status: 'Бронзовый',
    plane: '2021-07-13T21:05:20.344Z'
  },
  {
    id: 'e7939bc3-64c8-428e-901b-4ed4543037d8',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '3160441d-e7b9-43db-a0a1-203f094a4368',
    answer: 'конечно',
    status: 'Золотой',
    plane: '2021-07-13T21:05:20.344Z'
  },
  {
    id: 'f04fc8ca-614d-40f9-aef3-198fbcfac140',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    wordRuId: 'dc2ee330-31a5-4638-a94c-7261d0bc77aa',
    answer: 'меня',
    status: 'Золотой',
    plane: '2021-07-13T21:05:20.344Z',
    createdAt: '2021-06-27T08:26:54.548Z'
  },
  {
    id: '6905ca92-524e-4c4f-b33e-c55d87890ec7',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    wordEnId: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    wordRuId: 'dc2ee330-31a5-4638-a94c-7261d0bc77aa',
    answer: 'Cubb Globo',
    status: 'new',
    plane: '2021-07-13T21:05:20.344Z',
    createdAt: '2021-06-25T08:26:33.249Z'
  }
]

module.exports = logsSeed