const messageSeed = [
  {
    id: '33c75c9c-8965-4233-9eaa-28421d7f788e',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    mode: 'query',
    text: 'First query from first user',
    answer: ''
  },
  {
    id: '3661ecf2-0b1d-425c-8bc8-94846d373216',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    mode: 'query',
    text: 'Second query from first user',
    answer: ''
  },
  {
    id: '2c4f7401-156d-4246-b7d6-5eab77ee3dc7',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    mode: 'ask',
    text: 'First ask from first user',
    answer: ''
  },
  {
    id: '4429f707-0df9-4d41-8bd8-b47cbd5176ad',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    mode: 'query',
    text: 'First query from second user',
    answer: ''
  },
  {
    id: '370f0e4f-57b2-454c-80b3-40d5e1103ce3',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    mode: 'ask',
    text: 'First ask from second user',
    answer: ''
  },
  {
    id: 'd25a52a3-9ad6-4ffb-8f75-7a9db859c44f',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    mode: 'ask',
    text: 'Second ask from second user',
    answer: ''
  },
  {
    id: '69a56e18-4b11-4e28-8633-e025ef3da8e7',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    mode: 'message',
    text: 'Info message message message from second user',
    answer: ''
  },
  {
    id: '7c138b87-166c-4013-8b11-d2dac8ef67b6',
    userId: 'e7c6fb77-5f3b-4fee-bc6f-de42e82e1061',
    mode: 'message',
    text: 'Info message message message from third user',
    answer: ''
  }
]

const messReactionSeed = [
  {
    id: 'a349da2f-62c4-4cf7-9c9b-da4f0067b8d5',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    messageId: '2c4f7401-156d-4246-b7d6-5eab77ee3dc7',
    isLike: true
  },
  {
    id: 'eb698353-2dd1-429a-aee7-7797161e2e7a',
    userId: 'e7c6fb77-5f3b-4fee-bc6f-de42e82e1061',
    messageId: '2c4f7401-156d-4246-b7d6-5eab77ee3dc7',
    isLike: true
  },
  {
    id: '01443503-eafa-4a13-ae7a-bdb45e62ce4b',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    messageId: '2c4f7401-156d-4246-b7d6-5eab77ee3dc7',
    isLike: false
  },
  {
    id: 'f5b96a7b-e255-4b0e-8c7a-822b8cc9fa90',
    userId: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    messageId: '69a56e18-4b11-4e28-8633-e025ef3da8e7',
    isLike: true
  },
  {
    id: '5a3368b4-291a-4586-b95d-6a6a6c279427',
    userId: 'e7c6fb77-5f3b-4fee-bc6f-de42e82e1061',
    messageId: '69a56e18-4b11-4e28-8633-e025ef3da8e7',
    isLike: true
  },
  {
    id: '9086fcdb-b1f6-4b6d-98ce-e4e7bd40d13c',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    messageId: '33c75c9c-8965-4233-9eaa-28421d7f788e',
    isLike: false
  },
  {
    id: '030076ee-fb87-47c4-a380-e52df26e180b',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    messageId: '4429f707-0df9-4d41-8bd8-b47cbd5176ad',
    isLike: false
  }
]

module.exports = { messageSeed, messReactionSeed };