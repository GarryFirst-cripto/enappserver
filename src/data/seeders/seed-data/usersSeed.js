
const usersSeed = [
 {
    id: '34d9507f-83e4-41d1-8b7d-7b8c9ca553c5',
    email: 'none_Jhon@none.com',
    password: '11111',
    pushId: 'xxxxxx xxxxxx',
    sex: 'male'
  },
  {
    id: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    email: 'none_Tom@none.com',
    password: '22222',
    pushId: 'xxx xxx xxx xxx',
    sex: 'male'
  },
  {
    id: 'e7c6fb77-5f3b-4fee-bc6f-de42e82e1061',
    email: 'none_Mary@none.com',
    password: '33333',
    pushId: 'xxx xxx xxx xxx',
    sex: 'female'
  },
  {
    id: '4e28535b-147d-45d9-b0d1-95fdf2c07a09',
    email: 'sysAdmin',
    password: 'systemadminpwd',
    pushId: 'xxxxxx xxxxxx',
    sex: 'male'
  }
]

module.exports = usersSeed;
