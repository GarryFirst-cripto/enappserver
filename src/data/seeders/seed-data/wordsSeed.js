
const wordsEnSeed = [
  {
    id: 'd2cf28f5-7561-47ab-8474-8d360ac04bd3',
    Core: 'you',
    WordEn: 'you',
    WordEnFrq: 248560
  },
  {
    id: 'cb8b64d4-79a8-470b-a72e-59ef9158f871',
    Core: 'i',
    WordEn: 'i',
    WordEnFrq: 230248
  },
  {
    id: '8822c05b-dcc6-44ac-810f-9a1b25d83c87',
    Core: 'it',
    WordEn: 'it',
    WordEnFrq: 114253
  },
  {
    id: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    Core: 'this',
    WordEn: 'that',
    WordEnFrq: 85282
  },
  {
    id: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    Core: 'and',
    WordEn: 'and',
    WordEnFrq: 84974
  },
  {
    id: 'cd4ba79a-cdfb-48a5-a8d1-eb8735d3f4d3',
    Core: 'in',
    WordEn: 'in',
    WordEnFrq: 61318
  },
  {
    id: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    Core: 'what',
    WordEn: 'what',
    WordEnFrq: 59829
  },
  {
    id: '1b522a6b-57e2-4a70-a079-e76f951de2a1',
    Core: 'we',
    WordEn: 'we',
    WordEnFrq: 58472
  },
  {
    id: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    Core: 'i',
    WordEn: 'me',
    WordEnFrq: 55508
  }
]

const wordsRuSeed = [
  {
    id: 'f1861490-a1d7-43c2-a51f-6228160a206d',
    wordEnId: 'd2cf28f5-7561-47ab-8474-8d360ac04bd3',
    WordRu: 'ты, вы',
    Transcription: '[ju:], [ju], [jə]',
    Meaning: 'Описание сысла употребления слова you в переводе ты, вы',
    MeaningProc: 100
  },
  {
    id: '37edf198-4b0a-4a7f-a788-9818d34fd1f0',
    wordEnId: '1b522a6b-57e2-4a70-a079-e76f951de2a1',
    WordRu: 'я',
    Transcription: '0',
    Meaning: 'Описание сысла употребления слова i в переводе я',
    MeaningProc: 100
  },
  {
    id: '069abf2e-ff13-4a70-a620-af4f3550ded9',
    wordEnId: '8822c05b-dcc6-44ac-810f-9a1b25d83c87',
    WordRu: 'это',
    Transcription: '[it]',
    Meaning: 'Описание сысла употребления слова it в переводе это',
    MeaningProc: 100
  },
  {
    id: 'e4da70cb-32d8-4fee-a693-5f250650b72c',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    WordRu: 'это, этот (сущ)',
    Transcription: '[ðæt], [ðət], [ðt]',
    Meaning: 'Описание сысла употребления слова that в переводе это, этот (сущ)',
    MeaningProc: 35
  },
  {
    id: '8a785a2e-bbc8-40d0-a000-df170ccbe24e',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    WordRu: 'что (сущ)',
    Transcription: '[ðæt], [ðət], [ðt]',
    Meaning: 'Описание сысла употребления слова that в переводе что (сущ)',
    MeaningProc: 25
  },
  {
    id: '60c3488f-16e5-4740-81e6-001dcdfe77f4',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    WordRu: 'что',
    Transcription: '[ðæt], [ðət], [ðt]',
    Meaning: 'Описание сысла употребления слова that в переводе что',
    MeaningProc: 17
  },
  {
    id: '2bf61d29-c9b5-4d3a-97ab-1784117410c9',
    wordEnId: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    WordRu: 'и',
    Transcription: '[ænd], [ənd], [ən], [nd], [n]',
    Meaning: 'Описание сысла употребления слова and в переводе и',
    MeaningProc: 95
  },
  {
    id: '8fd3f16e-c724-4fc3-9c3a-3d51bfa1ee74',
    wordEnId: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    WordRu: 'а',
    Transcription: '[ænd], [ənd], [ən], [nd], [n]',
    Meaning: 'Описание сысла употребления слова and в переводе а',
    MeaningProc: 5
  },
  {
    id: '2959fb68-fced-4027-aa41-fa6e7ef1d4b5',
    wordEnId: 'cd4ba79a-cdfb-48a5-a8d1-eb8735d3f4d3',
    WordRu: 'в',
    Transcription: '[in]',
    Meaning: 'Описание сысла употребления слова in в переводе в',
    MeaningProc: 100
  },
  {
    id: '092dae68-165f-4774-9a6c-fed47a781224',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    WordRu: 'что',
    Transcription: '[wɔt]',
    Meaning: 'Описание сысла употребления слова what в переводе что',
    MeaningProc: 60
  },
  {
    id: '3160441d-e7b9-43db-a0a1-203f094a4368',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    WordRu: 'какой',
    Transcription: '[wɔt]',
    Meaning: 'Описание сысла употребления слова what в переводе какой',
    MeaningProc: 40
  },
  {
    id: '60c38981-1adb-4b82-8651-78dd99f232a7',
    wordEnId: '1b522a6b-57e2-4a70-a079-e76f951de2a1',
    WordRu: 'мы',
    Transcription: '[wi:]',
    Meaning: 'Описание сысла употребления слова we в переводе мы',
    MeaningProc: 100
  },
  {
    id: 'dc2ee330-31a5-4638-a94c-7261d0bc77aa',
    wordEnId: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    WordRu: 'меня, мне, мной',
    Transcription: '[mi:]',
    Meaning: 'Описание сысла употребления слова me в переводе меня, мне, мной',
    MeaningProc: 100
  }
]

const wordToWordSeed = [
  {
    wordEnId: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    wordRuId: 'dc2ee330-31a5-4638-a94c-7261d0bc77aa'
  },
  {
    wordEnId: '1b522a6b-57e2-4a70-a079-e76f951de2a1',
    wordRuId: '60c38981-1adb-4b82-8651-78dd99f232a7'
  },
  {
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '092dae68-165f-4774-9a6c-fed47a781224'
  },
  {
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '3160441d-e7b9-43db-a0a1-203f094a4368'
  },
  {
    wordEnId: 'cd4ba79a-cdfb-48a5-a8d1-eb8735d3f4d3',
    wordRuId: '2959fb68-fced-4027-aa41-fa6e7ef1d4b5'
  },
  {
    wordEnId: 'd2cf28f5-7561-47ab-8474-8d360ac04bd3',
    wordRuId: 'f1861490-a1d7-43c2-a51f-6228160a206d'
  },
  {
    wordEnId: '1b522a6b-57e2-4a70-a079-e76f951de2a1',
    wordRuId: '37edf198-4b0a-4a7f-a788-9818d34fd1f0'
  },
  {
    wordEnId: '8822c05b-dcc6-44ac-810f-9a1b25d83c87',
    wordRuId: '069abf2e-ff13-4a70-a620-af4f3550ded9'
  },
  {
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: 'e4da70cb-32d8-4fee-a693-5f250650b72c'
  },
  {
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: '8a785a2e-bbc8-40d0-a000-df170ccbe24e'
  },
  {
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: '60c3488f-16e5-4740-81e6-001dcdfe77f4'
  },
  {
    wordEnId: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    wordRuId: '2bf61d29-c9b5-4d3a-97ab-1784117410c9'
  },
  {
    wordEnId: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    wordRuId: '8fd3f16e-c724-4fc3-9c3a-3d51bfa1ee74'
  }
]

module.exports = { wordsEnSeed, wordsRuSeed, wordToWordSeed };
