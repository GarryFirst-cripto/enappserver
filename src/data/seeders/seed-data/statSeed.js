const statSeed = [
  {
    id: '47543c90-3850-4bef-b590-4254ef77cda0',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    experience: 35.5,
    intensity: 80.1,
    status: 'First user"s status',
    createdAt: '2021-06-20T10:02:09.199Z'
  },
  {
    id: '9ea0c4fc-b234-4f0f-885d-2a6739cf90dd',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    experience: 35.5,
    intensity: 80.1,
    status: 'Next user"s status',
    createdAt: '2021-06-23T10:02:09.199Z'
  },
  {
    id: '853b833f-f6fc-4f5c-bda1-47efe9059a69',
    userId: 'ee5d4ae1-54ec-4b54-b69f-7dfac09f97ba',
    experience: 65.5,
    intensity: 97,
    status: 'Last user"s status',
    createdAt: '2021-06-28T10:05:00.726Z'
  }
]

module.exports = statSeed;
