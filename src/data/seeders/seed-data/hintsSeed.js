const hintsSeed = [
  {
    id: 'b51e74fb-947d-4ea8-8f4c-f3a9d9088206',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '092dae68-165f-4774-9a6c-fed47a781224',
    Hint: 'Подсказка к слову what в переводе - что',
    HintRtg: 5
  },
  {
    id: '62e6a8ad-1de9-4e6d-ac72-817fa53a104f',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '092dae68-165f-4774-9a6c-fed47a781224',
    Hint: 'Вторая одсказка к слову what в переводе - что',
    HintRtg: 4,
    Sex: 'female'
  },
  {
    id: '9bf73cbd-b71b-4071-8c31-baf60b8e3eb5',
    wordEnId: 'dc5865df-37ab-4681-b6a5-1cac640fcf20',
    wordRuId: '092dae68-165f-4774-9a6c-fed47a781224',
    Hint: 'Треться одсказка к слову what в переводе - что',
    HintRtg: 3,
    Sex: 'male'
  },
  {
    id: '54488467-c6f5-4087-ae37-1dac2b20d029',
    wordEnId: 'cd4ba79a-cdfb-48a5-a8d1-eb8735d3f4d3',
    wordRuId: '2959fb68-fced-4027-aa41-fa6e7ef1d4b5',
    Hint: 'Подсказка к слову in в переводе - в',
    HintRtg: 8
  },
  {
    id: '14057cb0-1b85-4020-9a5d-2256b6dbc3d5',
    wordEnId: 'cd4ba79a-cdfb-48a5-a8d1-eb8735d3f4d3',
    wordRuId: '2959fb68-fced-4027-aa41-fa6e7ef1d4b5',
    Hint: 'Вторая одсказка к слову in в переводе - в',
    HintRtg: 1,
    Sex: 'female'
  },
  {
    id: 'dde2e875-8f7c-488f-a0eb-e61d745cd3b2',
    wordEnId: 'cd4ba79a-cdfb-48a5-a8d1-eb8735d3f4d3',
    wordRuId: '2959fb68-fced-4027-aa41-fa6e7ef1d4b5',
    Hint: 'Треться одсказка к слову in в переводе - в',
    HintRtg: 2,
    Sex: 'male'
  },
  {
    id: '4d6f76eb-d663-4d62-a6e3-cc7ebbdb1ea8',
    wordEnId: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    wordRuId: '8fd3f16e-c724-4fc3-9c3a-3d51bfa1ee74',
    Hint: 'Вторая одсказка к слову and в переводе - а',
    HintRtg: 3,
    Sex: 'female'
  },
  {
    id: '530ea77b-9275-4801-8184-4ae36840901b',
    wordEnId: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    wordRuId: '8fd3f16e-c724-4fc3-9c3a-3d51bfa1ee74',
    Hint: 'Треться одсказка к слову and в переводе - а',
    HintRtg: 6,
    Sex: 'male'
  },
  {
    id: '990a5711-d606-4cfe-9d83-e150e596ae02',
    wordEnId: '9c9c7996-2605-4d34-9071-9460c6a08c39',
    wordRuId: '8fd3f16e-c724-4fc3-9c3a-3d51bfa1ee74',
    Hint: 'Подсказка к слову and в переводе - а',
    HintRtg: 7
  },
  {
    id: '16cf1f54-ef1e-4b87-ad39-0d852d0ecaab',
    wordEnId: 'd2cf28f5-7561-47ab-8474-8d360ac04bd3',
    wordRuId: 'f1861490-a1d7-43c2-a51f-6228160a206d',
    Hint: 'Подсказка к слову you в переводе - ты, вы',
    HintRtg: 2
  },
  {
    id: 'facb15bc-cf6e-40ba-8471-2a11090dc7df',
    wordEnId: 'd2cf28f5-7561-47ab-8474-8d360ac04bd3',
    wordRuId: 'f1861490-a1d7-43c2-a51f-6228160a206d',
    Hint: 'Вторая одсказка к слову you в переводе - ты, вы',
    HintRtg: 0,
    Sex: 'female'
  },
  {
    id: 'bcbd7f3e-0b18-4816-bf66-34d905cc7bfa',
    wordEnId: 'd2cf28f5-7561-47ab-8474-8d360ac04bd3',
    wordRuId: 'f1861490-a1d7-43c2-a51f-6228160a206d',
    Hint: 'Треться одсказка к слову you в переводе - ты, вы',
    HintRtg: 2,
    Sex: 'male'
  },
  {
    id: '38c8313b-7fc5-459f-af7f-efa0027e3910',
    wordEnId: 'cb8b64d4-79a8-470b-a72e-59ef9158f871',
    wordRuId: '37edf198-4b0a-4a7f-a788-9818d34fd1f0',
    Hint: 'Подсказка к слову i в переводе - я',
    HintRtg: 3
  },
  {
    id: 'bb6c9d6e-4a42-4220-a034-f1a5f26a8c32',
    wordEnId: 'cb8b64d4-79a8-470b-a72e-59ef9158f871',
    wordRuId: '37edf198-4b0a-4a7f-a788-9818d34fd1f0',
    Hint: 'Вторая одсказка к слову i в переводе - я',
    HintRtg: 6,
    Sex: 'female'
  },
  {
    id: 'b2c31f8e-2f5b-4f86-ac1a-cc5016d6332c',
    wordEnId: 'cb8b64d4-79a8-470b-a72e-59ef9158f871',
    wordRuId: '37edf198-4b0a-4a7f-a788-9818d34fd1f0',
    Hint: 'Треться одсказка к слову i в переводе - я',
    HintRtg: 1,
    Sex: 'male'
  },
  {
    id: 'e70eb61e-2237-4605-88cd-780f2952ce6a',
    wordEnId: '8822c05b-dcc6-44ac-810f-9a1b25d83c87',
    wordRuId: '069abf2e-ff13-4a70-a620-af4f3550ded9',
    Hint: 'Подсказка к слову it в переводе - это',
    HintRtg: 0
  },
  {
    id: '26ce7c6f-d271-45fb-a3a3-f82e739b1fa9',
    wordEnId: '8822c05b-dcc6-44ac-810f-9a1b25d83c87',
    wordRuId: '069abf2e-ff13-4a70-a620-af4f3550ded9',
    Hint: 'Вторая одсказка к слову it в переводе - это',
    HintRtg: 0,
    Sex: 'female'
  },
  {
    id: '54839148-0cf7-4a4b-9f75-a8bcb4d8244a',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: 'e4da70cb-32d8-4fee-a693-5f250650b72c',
    Hint: 'Подсказка к слову that в переводе - это, этот (сущ)',
    HintRtg: 0
  },
  {
    id: '24db8a61-449d-49cd-b70b-0ea80ee2eb80',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: 'e4da70cb-32d8-4fee-a693-5f250650b72c',
    Hint: 'Вторая одсказка к слову that в переводе - это, этот (сущ)',
    HintRtg: 0,
    Sex: 'female'
  },
  {
    id: '561e49d5-443d-4dc6-a8fe-409742e3adcb',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: 'e4da70cb-32d8-4fee-a693-5f250650b72c',
    Hint: 'Треться одсказка к слову that в переводе - это, этот (сущ)',
    HintRtg: 0,
    Sex: 'male'
  },
  {
    id: 'b1009252-27cb-44dc-bc33-d1a6df90e104',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: '8a785a2e-bbc8-40d0-a000-df170ccbe24e',
    Hint: 'Подсказка к слову that в переводе - что (сущ)',
    HintRtg: 0
  },
  {
    id: '1c2ec012-763c-45bd-97a8-b588616b5a05',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: '8a785a2e-bbc8-40d0-a000-df170ccbe24e',
    Hint: 'Вторая одсказка к слову that в переводе - что (сущ)',
    HintRtg: 0,
    Sex: 'female'
  },
  {
    id: 'cc625705-c3a3-42b2-ace1-3a6b438d0c58',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: '8a785a2e-bbc8-40d0-a000-df170ccbe24e',
    Hint: 'Треться одсказка к слову that в переводе - что (сущ)',
    HintRtg: 0,
    Sex: 'male'
  },
  {
    id: 'eca44245-e489-4d42-a0df-f1d16ad38176',
    wordEnId: '659610b9-8b76-4974-adeb-f1183cf7ab2f',
    wordRuId: '60c3488f-16e5-4740-81e6-001dcdfe77f4',
    Hint: 'Вторая одсказка к слову that в переводе - что',
    HintRtg: 0,
    Sex: 'female'
  },
  {
    id: 'a53cc9d6-ca8b-4b46-b14b-fd21dde00fb3',
    wordEnId: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    wordRuId: 'dc2ee330-31a5-4638-a94c-7261d0bc77aa',
    Hint: 'Вторая одсказка к слову me в переводе - меня, мне, мной',
    HintRtg: 0,
    Sex: 'female'
  },
  {
    id: '9c968102-bf13-4fb4-ab2e-c7b70e63472e',
    wordEnId: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    wordRuId: 'dc2ee330-31a5-4638-a94c-7261d0bc77aa',
    Hint: 'Подсказка к слову me в переводе - меня, мне, мной',
    HintRtg: 2
  },
  {
    id: '9b28f9c8-61dc-474a-aae3-df3fe2fdbf5f',
    wordEnId: '7db86d89-0aa2-4f28-aca6-fc6457673d8d',
    wordRuId: 'dc2ee330-31a5-4638-a94c-7261d0bc77aa',
    Hint: 'Треться одсказка к слову me в переводе - меня, мне, мной',
    HintRtg: 1,
    Sex: 'male'
  }
]

module.exports = hintsSeed;
