const usersSeed = require('./seed-data/usersSeed');
const logsSeed = require('./seed-data/logsSeed');
const { wordsEnSeed, wordsRuSeed, wordToWordSeed } = require('./seed-data/wordsSeed');
const hintsSeed = require('./seed-data/hintsSeed');
const statSeed = require('./seed-data/statSeed');

module.exports = {
  up: async queryInterface => {
    try {
      const data = new Date();

      const usersMappedSeed = usersSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('users', usersMappedSeed, {});

      const wordsMappedSeed = wordsEnSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('wordEns', wordsMappedSeed, {});

      const wordsRuMappedSeed = wordsRuSeed.map(item => ({
        createdAt: data, ...item, service: 0
      }));
      await queryInterface.bulkInsert('wordRus', wordsRuMappedSeed, {});

      const hintsMappedSeed = hintsSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('hints', hintsMappedSeed, {});

      const logsMappedSeed = logsSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('loggs', logsMappedSeed, {});

      const wordsToWordMappedSeed = wordToWordSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('wordtowords', wordsToWordMappedSeed, {});

      const statMappedSeed = statSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('statistics', statMappedSeed, {});

    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('statistics', null, {});
      await queryInterface.bulkDelete('wordtowords', null, {});
      await queryInterface.bulkDelete('loggs', null, {});
      await queryInterface.bulkDelete('hints', null, {});
      await queryInterface.bulkDelete('comments', null, {});
      await queryInterface.bulkDelete('wordRus', null, {});
      await queryInterface.bulkDelete('wordEns', null, {});
      await queryInterface.bulkDelete('users', null, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  }
};
