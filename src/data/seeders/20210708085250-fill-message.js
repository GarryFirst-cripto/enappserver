const { messageSeed, messReactionSeed } = require('./seed-data/messageSeed');

module.exports = {
  up: async queryInterface => {
    try {
      const data = new Date();

      const messMappedSeed = messageSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('messages', messMappedSeed, {});

      const messMappedReactionSeed = messReactionSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('messReactions', messMappedReactionSeed, {});

    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('messReactions', null, {});
      await queryInterface.bulkDelete('messages', null, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  }

};
