module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        username: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        password: {
          allowNull: false,
          type: Sequelize.STRING
        },
        email: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        sex: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        age: Sequelize.DATE,
        pushId: Sequelize.STRING,
        status: Sequelize.STRING,
        sensay: Sequelize.STRING,
        verify: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('wordEns', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        Core: {
          allowNull: false,
          type: Sequelize.STRING
        },
        WordEn: {
          allowNull: false,
          type: Sequelize.STRING
        },
        WordEnFrq: Sequelize.FLOAT,
        status: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('wordRus', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        WordRu: {
          allowNull: false,
          type: Sequelize.STRING
        },        
        Transcription: Sequelize.STRING,
        Meaning: Sequelize.STRING,
        MeaningProc: Sequelize.FLOAT,
        audio: Sequelize.STRING,
        service: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('hints', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        Hint: Sequelize.STRING,
        Index: Sequelize.INTEGER,
        Sex: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('loggs', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        last: Sequelize.DATE,
        lastPlane: Sequelize.DATE,
        lastRepeat: Sequelize.DATE,
        answer: {
          allowNull: false,
          type: Sequelize.STRING
        },
        status: {
          allowNull: false,
          type: Sequelize.STRING
        },
        plane: {
          allowNull: false,
          type: Sequelize.DATE
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('statistics', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        experience: Sequelize.FLOAT,
        intensity: Sequelize.FLOAT,
        status: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('statistics', { transaction }),
      queryInterface.dropTable('loggs', { transaction }),
      queryInterface.dropTable('users', { transaction }),
      queryInterface.dropTable('hints', { transaction }),
      queryInterface.dropTable('wordRus', { transaction }),
      queryInterface.dropTable('wordEns', { transaction }),
    ]))
};
