module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('loggs', 'userId', {
        type: Sequelize.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('loggs', 'wordEnId', {
        type: Sequelize.UUID,
        references: {
          model: 'wordEns',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('loggs', 'wordRuId', {
        type: Sequelize.UUID,
        references: {
          model: 'wordRus',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('hints', 'wordEnId', {
        type: Sequelize.UUID,
        references: {
          model: 'wordEns',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('hints', 'wordRuId', {
        type: Sequelize.UUID,
        references: {
          model: 'wordRus',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('statistics', 'userId', {
        type: Sequelize.UUID,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.addColumn('wordRus', 'wordEnId', {
        type: Sequelize.UUID,
        references: {
          model: 'wordEns',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      }, { transaction }),
      queryInterface.createTable('wordtowords', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        wordEnId: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'wordEns',
            key: 'id'
          }
        },
        wordRuId: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'wordRus',
            key: 'id'
          }
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })
    ])),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('wordtowords', { transaction }),
      queryInterface.removeColumn('statistics', 'userId', { transaction }),
      queryInterface.removeColumn('loggs', 'userId', { transaction }),
      queryInterface.removeColumn('loggs', 'wordEnId', { transaction }),
      queryInterface.removeColumn('loggs', 'wordRuId', { transaction }),
      queryInterface.removeColumn('hints', 'wordEnId', { transaction }),
      queryInterface.removeColumn('hints', 'wordRuId', { transaction }),
      queryInterface.removeColumn('wordRus', 'wordEnId', { transaction }),
    ]))
};
