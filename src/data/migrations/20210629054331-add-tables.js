module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('comments', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        wordEnId: {
          type: Sequelize.UUID,
          references: {
            model: 'wordEns',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        wordRuId: {
          type: Sequelize.UUID,
          references: {
            model: 'wordRus',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        Comment: Sequelize.STRING,
        Usage: Sequelize.STRING,
        Index: Sequelize.INTEGER,
        Sex: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('grouptags', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        groupName: {
          allowNull: false,
          type: Sequelize.STRING
        },
        Info: Sequelize.STRING,
        Index: Sequelize.INTEGER,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.addColumn('wordEns', 'groupTagList', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('wordEns', 'ownerId', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.addColumn('wordRus', 'ownerId', {
        type: Sequelize.STRING
      }, { transaction }),
      queryInterface.renameColumn('hints', 'Index', 'HintRtg')
    ]))),

  down: (queryInterface) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.renameColumn('hints', 'HintRtg', 'Index'),
      queryInterface.removeColumn('wordRus', 'ownerId', { transaction }),
      queryInterface.removeColumn('wordEns', 'ownerId', { transaction }),
      queryInterface.removeColumn('wordEns', 'groupTagList', { transaction }),
      queryInterface.dropTable('grouptags', { transaction }),
      queryInterface.dropTable('comments', { transaction }),
    ]))
};
