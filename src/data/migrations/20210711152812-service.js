module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('wordEns', 'service', {
        type: Sequelize.INTEGER,
      }, { transaction }),
      queryInterface.bulkUpdate('wordEns', { service: 0 }, {}, { transaction }),
      queryInterface.bulkUpdate('wordRus', { service: 0 }, {}, { transaction }),
      queryInterface.removeColumn('loggs', 'last', { transaction }),
      queryInterface.removeColumn('loggs', 'lastPlane', { transaction }),
      queryInterface.removeColumn('loggs', 'lastRepeat', { transaction }),
      queryInterface.addColumn('loggs', 'state', {
        type: Sequelize.INTEGER,
      }, { transaction }),
      queryInterface.removeColumn('loggs', 'status', { transaction }),
      queryInterface.addColumn('loggs', 'wordstate', {
        type: Sequelize.STRING,
      }, { transaction }),
      queryInterface.addColumn('loggs', 'wordzone', {
        type: Sequelize.INTEGER,
      }, { transaction }),
      queryInterface.addColumn('loggs', 'index', {
        type: Sequelize.INTEGER,
      }, { transaction }),
      queryInterface.addColumn('loggs', 'timing', {
        type: Sequelize.INTEGER,
      }, { transaction }),
      queryInterface.addColumn('loggs', 'experience', {
        type: Sequelize.INTEGER,
      }, { transaction }),
      queryInterface.changeColumn('loggs', 'plane', {
        allowNull: true,
        type: Sequelize.DATE
      }, { transaction }),
      queryInterface.removeColumn('loggs', 'answer', { transaction }),
      queryInterface.addColumn('loggs', 'answer', {
        type: Sequelize.BOOLEAN
      }, { transaction })
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('wordEns', 'service', { transaction }),
      queryInterface.removeColumn('loggs', 'state', { transaction }),
      queryInterface.removeColumn('loggs', 'wordstate', { transaction }),
      queryInterface.removeColumn('loggs', 'wordzone', { transaction }),
      queryInterface.removeColumn('loggs', 'index', { transaction }),
      queryInterface.removeColumn('loggs', 'timing', { transaction }),
      queryInterface.removeColumn('loggs', 'experience', { transaction }),
      queryInterface.addColumn('loggs', 'status', {
        type: Sequelize.STRING,
      }, { transaction }),
      queryInterface.addColumn('loggs', 'lastRepeat', {
        type: Sequelize.DATE,
      }, { transaction }),
      queryInterface.addColumn('loggs', 'lastPlane', {
        type: Sequelize.DATE,
      }, { transaction }),
      queryInterface.addColumn('loggs', 'last', {
        type: Sequelize.DATE,
      }, { transaction })      
    ]))
};