module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'pushId', { transaction }),
      queryInterface.removeColumn('users', 'username', { transaction }),
      queryInterface.addColumn('users', 'point', {
        type: Sequelize.INTEGER,
      }, { transaction }),
      queryInterface.renameColumn('users', 'sensay', 'ownname', { transaction }),
      queryInterface.changeColumn('users', 'age', {
        type: Sequelize.STRING
      }, { transaction })
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'point', { transaction }),
      queryInterface.renameColumn('users', 'ownname', 'sensay', { transaction }),
      queryInterface.addColumn('users', 'pushId', {
        type: Sequelize.STRING,
      }, { transaction }),
      queryInterface.addColumn('users', 'username', {
        type: Sequelize.STRING,
      }, { transaction })
    ]))
};
