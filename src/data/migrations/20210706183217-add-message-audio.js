module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.createTable('messages', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        userId: {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        mode: Sequelize.STRING,
        text: Sequelize.STRING,
        answer: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('messReactions', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        userId: {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        messageId: {
          type: Sequelize.UUID,
          references: {
            model: 'messages',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        isLike: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('audios', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        wordId: {
          type: Sequelize.UUID,
          references: {
            model: 'wordEns',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        language: Sequelize.STRING,
        voice: Sequelize.STRING,
        sex: Sequelize.STRING,
        publicLink: Sequelize.STRING,
        link: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.removeColumn('wordRus', 'audio', { transaction }),
      queryInterface.addColumn('loggs', 'mode', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
    ])),

  down: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('loggs', 'mode', { transaction }),
      queryInterface.dropTable('audios', { transaction }),
      queryInterface.dropTable('messReactions', { transaction }),
      queryInterface.dropTable('messages', { transaction }),
      queryInterface.addColumn('wordRus', 'audio', {
        type: Sequelize.STRING
      }, { transaction }),
    ]))
};