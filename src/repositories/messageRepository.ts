import { Sequelize } from 'sequelize';
import { MessageModel, MessReactionModel, UserModel } from './Models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';

const queryText = (bool:boolean) => `(SELECT COUNT(*) FROM "messReactions" WHERE "messReactions"."messageId" = "message"."id" and "messReactions"."isLike" = ${bool})`;

class MessageRepository extends BaseRepository {

  async getMessages(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [Sequelize.literal(queryText(true)), 'likes'],
          [Sequelize.literal(queryText(false)), 'dislikes']          
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username', 'email', 'sex']
      },{
        model: MessReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'message.id',
        'user.id',
        'messReactions.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  async getMessageById(id: string) {
    return this.model.findOne({
      where: { id },
      attributes: {
        include: [
          [Sequelize.literal(queryText(true)), 'likes'],
          [Sequelize.literal(queryText(false)), 'dislikes']          
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username', 'email', 'sex']
      },{
        model: MessReactionModel,
        attributes: ['id', 'isLike', 'createdAt'],
        duplicating: false,
        include: {
          model: UserModel,
          attributes: ['id', 'username', 'email', 'sex']
        }
      }],
      group: [
        'message.id',
        'user.id',
        'messReactions.id',
        'messReactions->user.id'
      ],
    });
  }

  async createMessage(userId:string, data: any){
    data.userId = userId;
    return await this.create(data);
  }
  
  async updateMessage(userId:string, id:string, data: any){
    data.userId = userId;
    return await this.updateById(id, data);
  }

}

export default new MessageRepository(MessageModel);
