import { DataTypes } from "sequelize";
import { IUserModel } from "../interfaces/dataInterfaces";

export default (sequelize: { define: (arg0: string, arg1: IUserModel, arg2: {}) => any; }) => {
  const User = sequelize.define('user', {
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
      get() {
        try {
          return this.getDataValue('email').toLowerCase();
        } catch {
          return null;
        }
      },
      set(value: string) {
        return this.setDataValue('email', value.toLowerCase());
      }
    },
    sex: {
      allowNull: false,
      type: DataTypes.STRING
    },
    ownname: DataTypes.STRING,
    age: DataTypes.STRING,
    status: DataTypes.STRING,
    verify: DataTypes.STRING,
    point: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
