import { DataTypes } from "sequelize";
import { IWordEnModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IWordEnModel, arg2: {}) => any; }) => {
  const Word = orm.define('wordEn', {
    Core: {
      allowNull: false,
      type: DataTypes.STRING
    },
    WordEn: {
      allowNull: false,
      type: DataTypes.STRING
    },
    WordEnFrq: DataTypes.NUMBER,
    status: DataTypes.STRING,
    groupTagList: DataTypes.STRING,
    ownerId: DataTypes.STRING,
    service: DataTypes.NUMBER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Word;
};
