import sequelize from '../../data/db/connection';
import associate from './associations';
import userModel from './user';
import wordEnModel from './wordEn';
import wordRuModel from './wordRu';
import commentModel from './comment';
import groupTagModel from './groupTags';
import hintModel from './hint';
import loggModel from './logg';
import wordToWordModel from './wordToWord';
import statisticModel from './statistics';
import messageModel from './message';
import messReactionModel from './messReaction';
import audioModel from './audio';

const User = userModel(sequelize);
const WordEn = wordEnModel(sequelize);
const WordRu = wordRuModel(sequelize);
const Comment = commentModel(sequelize);
const GroupTag = groupTagModel(sequelize);
const Hint = hintModel(sequelize);
const Logg = loggModel(sequelize);
const WordToWord = wordToWordModel(sequelize);
const Statistic = statisticModel(sequelize);
const Message = messageModel(sequelize);
const MessReaction = messReactionModel(sequelize);
const Audio = audioModel(sequelize);

associate({
  User,
  WordEn,
  WordRu,
  Hint,
  Comment,
  Logg,
  Statistic,
  Message,
  MessReaction,
  Audio
});

export {
  User as UserModel,
  WordEn as WordEnModel,
  WordRu as WordRuModel,
  Hint as HintModel,
  Comment as CommentModel,
  GroupTag as GroupTagModel,
  Logg as LogModel,
  WordToWord as WordToWordModel,
  Statistic as StatisticModel,
  Message as MessageModel,
  MessReaction as MessReactionModel,
  Audio as AudioModel
};
