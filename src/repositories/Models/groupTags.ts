import { DataTypes } from "sequelize";
import { IGroupTagModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IGroupTagModel, arg2: {}) => any; }) => {
  const Word = orm.define('grouptags', {
    groupName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    Info: DataTypes.STRING,
    Index:  DataTypes.NUMBER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Word;
};