import { DataTypes } from "sequelize";
import { ICommentModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: ICommentModel, arg2: {}) => any; }) => {
  const Word = orm.define('comment', {
    wordEnId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    wordRuId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    Comment: DataTypes.STRING,
    Usage: DataTypes.STRING,
    Index: DataTypes.NUMBER,
    Sex: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Word;
};