import { DataTypes } from "sequelize";
import { IMessageModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IMessageModel, arg2: {}) => any; }) => {
  const Message = orm.define('message', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    mode: DataTypes.STRING,
    text: DataTypes.STRING,
    answer: DataTypes.STRING,
    // likes: DataTypes.NUMBER,
    // dislikes: DataTypes.NUMBER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Message;
};
