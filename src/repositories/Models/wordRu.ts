import { DataTypes } from "sequelize";
import { IWordRuModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IWordRuModel, arg2: {}) => any; }) => {
  const Word = orm.define('wordRu', {
    wordEnId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    WordRu: {
      allowNull: false,
      type: DataTypes.STRING
    },
    Transcription: DataTypes.STRING,
    Meaning: DataTypes.STRING,
    MeaningProc: DataTypes.NUMBER,
    ownerId: DataTypes.STRING,
    service: DataTypes.NUMBER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Word;
};
