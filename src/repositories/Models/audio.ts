import { DataTypes } from "sequelize";
import { IAudioModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IAudioModel, arg2: {}) => any; }) => {
  const Audio = orm.define('audio', {
    wordId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    language: DataTypes.STRING,
    voice: DataTypes.STRING,
    sex: DataTypes.STRING,
    publicLink: DataTypes.STRING,
    link: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Audio;
};
