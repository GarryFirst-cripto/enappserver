import { DataTypes } from "sequelize";
import { IStatisticModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IStatisticModel, arg2: {}) => any; }) => {
  const Statistic = orm.define('statistic', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    experience: DataTypes.FLOAT,
    intensity: DataTypes.FLOAT,
    status: DataTypes.STRING, 
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Statistic;
};
