import { DataTypes } from "sequelize";
import { IMessReaction } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IMessReaction, arg2: {}) => any; }) => {
  const MessReaction = orm.define('messReaction', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    messageId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    isLike: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return MessReaction;
};
