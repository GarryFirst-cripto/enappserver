import { DataTypes } from "sequelize";
import { ILogModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: ILogModel, arg2: {}) => any; }) => {
  const Logg = orm.define('logg', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    wordEnId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    wordRuId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    answer: {
      allowNull: false,
      type: DataTypes.STRING
    },
    wordstate: DataTypes.STRING,
    wordzone: DataTypes.INTEGER,
    plane: DataTypes.DATE,
    timing: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    experience: DataTypes.INTEGER,
    mode: DataTypes.BOOLEAN,
    index: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Logg;
};
