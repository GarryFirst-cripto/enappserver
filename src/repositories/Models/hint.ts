import { DataTypes } from "sequelize";
import { IHintModel } from "../interfaces/dataInterfaces";

export default (orm: { define: (arg0: string, arg1: IHintModel, arg2: {}) => any; }) => {
  const Word = orm.define('hint', {
    wordEnId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    wordRuId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    Hint: DataTypes.STRING,
    HintRtg: DataTypes.NUMBER,
    Sex: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Word;
};