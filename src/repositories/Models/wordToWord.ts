import { DataTypes } from 'sequelize'
import { IWordToWordModel } from '../interfaces/dataInterfaces'

export default (orm: { define: (arg0: string, arg1: IWordToWordModel, arg2: {}) => any }) => {
  const WordToWord = orm.define('wordtoword', {
    wordEnId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    wordRuId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  },{},);

  return WordToWord
}
