import Sequelize, { Op } from 'sequelize';
import { WordEnModel, WordRuModel, CommentModel, HintModel, WordToWordModel, LogModel, AudioModel } from './Models/index';
import BaseRepository from './baseRepository';
import WordRuRepository from './wordsRuRepository';
import { IListFilter, IComplexFilter } from '../api/interfaces/interfaces';
import { IWordData, IDataValues, IWordEnValue, IWordRu, IWordEn } from './interfaces/repoInterfaces';

interface IStatusFilter {
  userId: string;
  status?: string;
  answer?: string;
}

class WordRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, group, ...rest } = filter;
    const dopFilter = group ? { groupTagList: { [Sequelize.Op.substring]: group }, ...rest } : rest;
    return await this.model.findAll({
      where: dopFilter,
      order: ['WordEn'],
      attributes: {
        exclude: ['ownerId', 'service']
      },
      offset,
      limit
    });
  }

  async getAudioList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const dopFilter = { ...rest };
    return await this.model.findAll({
      where: dopFilter,
      order: ['WordEn'],
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: {
        model: AudioModel
      },
      offset,
      limit
    });
  }

  async getAudioListById(id: string) {
    return await this.model.findOne({
      where: { id },
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: {
        model: AudioModel
      }
    });
  }

  userSexAny:string = null;
  // dataInclude = (userId:string, userSex:string) => [{
  //   model: CommentModel,
  //   order: [['index', 'DESC']],
  //   where: { Sex: { [Op.or]: { [Op.like]: userSex, [Op.eq]: this.userSexAny} }},
  //   required: false
  // },{
  //   model: HintModel,
  //   order: [['index', 'DESC']],
  //   where: { Sex: { [Op.or]: { [Op.like]: userSex, [Op.eq]: this.userSexAny} }},
  //   required: false
  // },{
  //   model: LogModel,
  //   required: false,
  //   where: { userId },
  // }]
  dataInclude = (userId:string, userSex:string) => [{
    model: HintModel,
    // order: [['HintRtg', 'DESC']],
    attributes: {
      exclude: ['id','wordEnId','wordRuId','HintRtg','createdAt','updatedAt','ownerId', 'service']
    },
    where: { Sex: { [Op.or]: { [Op.like]: userSex, [Op.eq]: this.userSexAny} }},
    required: false
  },{
    model: LogModel,
    required: false,
    where: { userId },
  }]

  testLogg = (item:any[]):boolean => {
    return item.length > 0 && item[0].state < 16 && item[0].mode === true;
  }
  testKnow = (item:any[]):boolean => {
    return item.length > 0 && (item[0].state === 16 || (item[0].mode === false && item[0].answer === true));
  }

  async getWordsFullList(userId: string, userSex: string, filter: IComplexFilter, mode: string) {
    const { count: limit, from: offset } = filter;
    if (mode) {
      const list = await this.model.findAll({
        attributes: {
          exclude: ['status','groupTagList','createdAt','updatedAt','ownerId', 'service']
        },
        order: [['WordEnFrq', 'desc']], // 'WordEn'],
        include: [{
          model: WordRuModel,
          attributes: {
            exclude: ['wordEnId','Meaning','service', 'ownerId','createdAt','updatedAt'],
            include: [
              [Sequelize.literal('"MeaningProc" * "WordEnFrq"'), 'waight_field'],
            ]
          },
          required: true,
          order: [Sequelize.literal('"MeaningProc" * "WordEnFrq"'), 'desc'],
          include: this.dataInclude(userId, userSex)
        },{
          model: AudioModel,
          attributes: {
            exclude: ['wordId','createdAt','updatedAt']
          },
        }]
      });
      for (let j = 0; j < list.length; j ++) {
        for (let i = 0; i < list[j].dataValues.wordRus.length; i++) {
          const loggs = list[j].dataValues.wordRus[i].loggs.sort((itemA: any, itemB: any) => {
            const result = itemB.index - itemA.index;
            if (result === 0) return itemB.mode - itemA.mode;
            return result;
          });
          list[j].dataValues.wordRus[i].loggs = loggs;
        }
      }

      const listNew = list.filter((item: any) => {
        for (let i = 0; i < item.wordRus.length; i++){
          if (item.wordRus[i].loggs.length > 0) return false;
        };
        return true;
      });
      const listLearn = list.filter((item: any) => {
        for (let i = 0; i < item.wordRus.length; i++){
          if (this.testLogg(item.wordRus[i].loggs)) return true;
        };
        return false;
      });
      listLearn.sort((itemA: any, itemB: any) => {
        let stateA = 0;
        let stateB = 0;
        itemA.wordRus.forEach((item: any) => {
          if (item.loggs.length > 0) stateA = Math.max(stateA, item.loggs[0].state)
        });
        itemB.wordRus.forEach((item: any) => {
          if (item.loggs.length > 0) stateB = Math.max(stateB, item.loggs[0].state)
        });
        return stateB - stateA;
      });
      const listKnow = list.filter((item: any) => {
        let result = false;
        for (let i = 0; i < item.wordRus.length; i++){
          if (this.testLogg(item.wordRus[i].loggs)) return false;
          if (this.testKnow(item.wordRus[i].loggs)) result = true;
        };
        return result;
      });
      
      listKnow.sort((itemA: IWordEn, itemB: IWordEn) => itemA.WordEn.localeCompare(itemB.WordEn));
      // if (offset && limit) {
      //   return {
      //     listNew: listNew.slice(offset, offset + limit),
      //     listLearn: listLearn.slice(offset, offset + limit),
      //     listKnow: listKnow.slice(offset, offset + limit),
      //   };
      // }
      return { listNew, listLearn, listKnow };
    } else {
      const list = await this.model.findAll({
        attributes: {
          exclude: ['ownerId', 'service']
        },
        order: ['WordEn'],
        include: [{
          model: WordRuModel,
          attributes: {
            exclude: ['service', 'ownerId'],
            include: [
              [Sequelize.literal('"MeaningProc" * "WordEnFrq"'), 'waight_field'],
            ]
          },
          required: true,
          order: [Sequelize.literal('"MeaningProc" * "WordEnFrq"'), 'desc'],
          include: this.dataInclude(userId, userSex)
        },{
          model: AudioModel
        }],
        offset,
        limit
      });
      for (let j = 0; j < list.length; j ++) {
        for (let i = 0; i < list[j].dataValues.wordRus.length; i++) {
          const loggs = list[j].dataValues.wordRus[i].loggs.sort((itemA: any, itemB: any) => {
            const result = itemB.index - itemA.index;
            if (result === 0) return itemB.mode - itemA.mode;
            return result;
          });
          list[j].dataValues.wordRus[i].loggs = loggs;
        }
      }
      return list;
    }
  }

  async getWordsComplex(userId: string, userSex: string, filter: IComplexFilter) {
    const { count: limit, from: offset, minWeight, maxWeight } = filter;
    const minValue = minWeight ? minWeight * 100 : 0;
    const maxValue = maxWeight ? maxWeight * 100 : Number.MAX_SAFE_INTEGER;
    const list = await this.model.findAll({
      // order: [Sequelize.fn('RANDOM')],
      attributes: {
        exclude: ['ownerId', 'service']
        // include: [
        //   [Sequelize.literal('ROW_NUMBER() OVER (ORDER BY "WordEn")'), 'raw'],
        // ]
      },
      // order: [Sequelize.literal('"wordRus"."MeaningProc" * "WordEnFrq"')],
      // order: [[ WordRuModel, 'MeaningProc', 'asc']],
      order: [[ WordRuModel, Sequelize.literal('"MeaningProc" * "WordEnFrq"'), 'asc']],
      include: [{
        model: WordRuModel,
        attributes: {
          exclude: ['service', 'ownerId'],
          include: [
            [Sequelize.literal('"MeaningProc" * "WordEnFrq"'), 'waight_field'],
          ]
        },
        required: true,
        // order: [['MeaningProc', 'DESK']],
        order: [Sequelize.literal('"MeaningProc" * "WordEnFrq"')],
        // attributes: {
        //   include: [
        //     [(<number> <unknown>Sequelize.literal(`${minValue} / "WordEnFrq"`)), 'test_field_from'],
        //     [(<number> <unknown>Sequelize.literal(`${maxValue} / "WordEnFrq"`)), 'test_field_upto']
        //   ]
        // },
        where: { MeaningProc: { [Op.between]: [(<number> <unknown>Sequelize.literal(`${minValue} / "WordEnFrq"`)), (<number> <unknown>Sequelize.literal(`${maxValue} / "WordEnFrq"`))] } },
        include: this.dataInclude(userId, userSex)
      },{
        model: AudioModel
      }],
      offset,
      limit
    });
    return list;
  }

  async getWordWord(wordEnId: string, wordRuId: string) {
    const result = await this.model.findOne({
      where: { id: wordEnId },
      include: {
        model: WordRuModel,
        where: { id: wordRuId }
      }
    })
    return result;
  }

  async getVocabulary(userId: string, userSex: string) {
    const list = await this.model.findAll({
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: [{
        model: WordRuModel,
        attributes: {
          exclude: ['service', 'ownerId'],
        },
        required: true,
        include: [{
          model: LogModel,
          order: [['index', 'DESC']],
          required: true,
          where: { userId, mode: { [Op.eq]: true } }
        },{
          model: HintModel,
          order: [['index', 'DESC']],
          where: { Sex: { [Op.or]: { [Op.like]: userSex, [Op.eq]: this.userSexAny} }},
          required: false
        },{
          model: CommentModel,
          order: [['index', 'DESC']],
          where: { Sex: { [Op.or]: { [Op.like]: userSex, [Op.eq]: this.userSexAny} }},
          required: false
        }]
      },{
        model: AudioModel
      }]
    });
    return list;
  }

  async getShortVocabulary(userId: string) {
    const list = await this.model.findAll({
      attributes: ['id'],
      include: [{
        model: WordRuModel,
        attributes: ['id'],
        required: true,
        include: [{
          model: LogModel,
          attributes: ['id', 'answer', 'mode', 'state', 'index'],
          order: [['index', 'DESC']],
          required: false,
          where: { userId } // , mode: { [Op.eq]: true } }
        }]
      }]
    });
    return list;
  }

  async getNewWords(userId: string, userSex: string, filter: IListFilter) {
    const { from: offset, count: limit, group } = filter;
    const dopFilter = group ? { groupTagList: { [Sequelize.Op.substring]: group } } : {};
    const query = `(SELECT COUNT(*) FROM "loggs" WHERE "loggs"."wordRuId" = "wordRus"."id" and "loggs"."userId" = '${userId}')`;
    const list = await this.model.findAll({
      where:  dopFilter,
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: [{
        model: WordRuModel,
        required: true,
        order: [['MeaningProc', 'DESK']],
        attributes: {
          exclude: ['service', 'ownerId']
          // include: [
          //   // [ Sequelize.literal(query), 'loggss' ],
          //   [Sequelize.literal(`(SELECT COUNT(*) FROM "loggs" WHERE "loggs"."wordRuId" = "wordRus"."id"
          //     and "loggs"."userId" = '${userId}')`), 'mycount'],
          // ]
        },
        where: { service: { [Op.eq]: (<number> <unknown>Sequelize.literal(query)) }},
        include: this.dataInclude(userId, userSex)
      },{
        model: AudioModel
      }],
      offset,
      limit
    });
    return list;
  }

  async getWordsByStatus(userId: string, filter: IListFilter) {
    const { from: offset, count: limit, status, answer, group } = filter;
    const dopFilter = group ? { groupTagList: { [Sequelize.Op.substring]: group } } : {};
    const statusFilter: IStatusFilter = { userId };
    if (status) statusFilter.status = status;
    if (answer) statusFilter.answer = answer;
    return this.model.findAll({
      where: dopFilter,
      order: ['WordEn'],
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: [{
        model: WordRuModel,
        attributes: {
          exclude: ['service', 'ownerId']
        },
        required: true,
        include: [{
          model: LogModel,
          required: true,
          where: statusFilter
        }]
      }],
      offset,
      limit
    });
  }

  async getWordsList(userId: string, userSex: string, filter: IListFilter) {
    const { from: offset, count: limit, required, group, translations } = filter;
    const dopFilter = group ? { groupTagList: { [Sequelize.Op.substring]: group } } : {};
    const reqq = required ? required : false;
    const result = await this.model.findAll({
      where: dopFilter,
      order: ['WordEn'],
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: [{
        model: WordRuModel,
        attributes: {
          exclude: ['service', 'ownerId']
        },
        required: false,
        order: [['MeaningProc', 'DESK']],
        include: this.dataInclude(userId, userSex)
      },{
        model: AudioModel
      }],
      offset,
      limit
    });
    if (translations > 0) {
      const idArray:string[] = [];
      result.forEach((item:IDataValues) => {
        item.dataValues.wordRus.forEach(subitem => {
          idArray.push(subitem.dataValues.id);
        });
      });
      let cau = limit * translations;
      const wordsRu = await WordRuRepository.getTranslations(cau, idArray);
      cau = 0; 
      result.forEach((item:IDataValues) => {
        item.dataValues.translations = [];
        for (let i = 0; i < translations; i++) {
          item.dataValues.translations.push(wordsRu[cau].dataValues.WordRu);
          cau ++;
          if (cau >= wordsRu.length) cau = 0;
        }
      });
    }
    return result;
  }

  async getWordById(userId: string, userSex: string, id: string) {
    const result = await this.model.findOne({
      where: { id },
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: [{
        model: WordRuModel,
        attributes: {
          exclude: ['service', 'ownerId']
        },
        required: false,
        order: [['MeaningProc', 'DESK']],
        include: this.dataInclude(userId, userSex)
      },{
        model: AudioModel
      }]
    });
    return result;
  }

  async getWordList(userId: string, userSex: string, idArray:string[]) {
    const result = await this.model.findAll({
      where: { id: { [Sequelize.Op.in]: idArray } },
      order: ['WordEn'],
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: [{
        model: WordRuModel,
        attributes: {
          exclude: ['service', 'ownerId']
        },
        required: false,
        order: [['MeaningProc', 'DESK']],
        include: this.dataInclude(userId, userSex)
      },{
        model: AudioModel
      }]
    });
    return result;
  }

  async getDataById(userId: string, userSex: string, id: string, wordRuId: string) {
    const result = await this.model.findOne({
      where: { id },
      attributes: {
        exclude: ['ownerId', 'service']
      },
      include: [{
        model: WordRuModel,
        attributes: {
          exclude: ['service', 'ownerId', 'Transcription']
        },
        required: false,
        order: [['MeaningProc', 'DESK']],
        where: { id: wordRuId },
        include: this.dataInclude(userId, userSex)
      }]
    });
    return result;
  }

  control(data: IWordData) {
    let result = '';
    if (!data.Core) result = 'Core ';
    if (!data.WordEn) result += 'WordEn ';
    if (!data.WordRu) result += 'WordRu ';
    if (result) result = 'No data for fields : '+result;
    return result;
  }

  async createEnWord(word: IWordEnValue):Promise<IWordEnValue> {
    const { ownerId, Core, WordEn, WordEnFrq, groupTagList } = word;
    const oldWord = await this.model.findOne({ where: { ownerId } })
    if (oldWord) {
      await this.updateById(oldWord.id, word);
      return oldWord;
    }
    const existWord = await this.model.findOne({ where: { Core, WordEn } });
    if (existWord) return existWord;
    const newWord = <IWordEnValue> <unknown> await this.model.create({ ownerId, Core, WordEn, WordEnFrq, groupTagList, service: 0 }); // this.create(word);
    return newWord;
  }

  async createNewEnWord(wordData: IWordData) {
    const message = this.control(wordData);
    if (message) return { status: 400,  message };
    const { ID: ownerId, Core, WordEn, WordEnFrq, groupTagList } = wordData;
    const { id: wordEnId } = await this.createEnWord({ ownerId, Core, WordEn, WordEnFrq, groupTagList, service: 0 });
    const { WordRu, Transcription, Meaning, MeaningProc, Hint1, Hint2, Hint3, HintRtg, RuComment: Comment, RuUsage: Usage } = wordData;
    const { id: wordRuId } = await WordRuRepository.createRuWords({ wordEnId, ownerId, WordRu, Transcription, Meaning, MeaningProc, Hint1, Hint2, Hint3, HintRtg, Comment, Usage });
    const link = await WordToWordModel.findOne({ where: { wordEnId, wordRuId } });
    if (!link) {
      await WordToWordModel.create({ wordEnId, wordRuId });
      return { status: 200,  message: 'Ok' };
    }
    return { status: 200, message: 'Already exists'};
  }

  async delete(id: string) {
    await WordToWordModel.destroy({ where: { wordEnId: id }});
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

  async removeEmptyWords() {
    const query = '(SELECT COUNT(*) FROM "wordRus")';
    const list = await this.model.findAll({
      attributes: ['id'],
      where: { service: { [Op.eq]: (<number> <unknown>Sequelize.literal(query)) }},
      include: [{
        model: WordRuModel,
        required: false,
        attributes: ['id', 'wordEnId']
      }]
    });
    return list;
  }

}

export default new WordRepository(WordEnModel);
