import { CommentModel } from './Models/index';
import BaseRepository from './baseRepository';
import { ICommentsData } from './interfaces/repoInterfaces';

class CommentRepository extends BaseRepository {
  model: any;

  async getList(filter: object) {
    return await this.model.findAll({ where: filter });
  }

  async deleteWordComments(wordRuId: string) {
    const result = await this.model.destroy({ where: { wordRuId } });
    return { result };
  }

  async appendComment(commentData: ICommentsData) {
    const { wordEnId, wordRuId, Comment, Usage, Index } = commentData;
    if (Comment || Usage) {
      const oldItem = await this.model.findOne({ where : { wordEnId, wordRuId }});
      if (!oldItem) this.create({ wordEnId, wordRuId, Comment, Usage, Index })
      else this.updateById(oldItem.id, { Comment, Usage, Index });
    }
  }

}

export default new CommentRepository(CommentModel);