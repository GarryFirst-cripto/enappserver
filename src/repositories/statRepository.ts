import { StatisticModel } from './Models/index';
import BaseRepository from './baseRepository';

class StatRepository extends BaseRepository {

  async getList(userId:string, filter: object) {
    return await this.model.findAll({ where: { ...filter, userId }});
  }

  async getStat(filter: object) {
    return await this.model.findAll({ where: filter });
  }

  async deleteUserStat(userId: string) {
    const result = await this.model.destroy({ where: { userId } });
    return { result };
  }

}

export default new StatRepository(StatisticModel);