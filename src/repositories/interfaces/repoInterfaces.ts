import { IHintModel, ILogModel } from "./dataInterfaces";

export interface IWordEn {
  id?: string;
  Core: string; 
  WordEn: string;
  WordEnFrq: number;
  status?: string;
  createdAt?: Date;
  updatedAt?: Date;
  wordRus?: IWordRu[];
  translations?: string[];
}

export interface IWordData {
  ID: string;
  Core: string; 
  WordEn: string;
  WordEnFrq: number; 
  Transcription: string; 
  Meaning: string;
  MeaningProc: number;
  WordRu: string;
  RuComment: string;
  RuUsage: string;
  Hint1: string;
  Hint2: string;
  Hint3: string;
  HintRtg: number;
  groupTagList: string;
}

export interface IWordDataRu {
  id?: string;
  ownerId: string;
  wordEnId: string;
  WordRu: string;
  Transcription: string;
  Meaning: string;
  MeaningProc: number;
  Hint1: string;
  Hint2: string;
  Hint3: string;
  HintRtg: number;
  Comment: string;
  Usage: string;
  service?: number;
}

export interface IWordRu {
  id: string;
  WordRu: string;
  Transcription: string;
  Meaning: string;
  MeaningProc: number;
  createdAt?: Date;
  updatedAt?: Date;
  hints?: IHintModel[];
  loggs?: ILogModel[];
}

interface IRuDataValues {
  dataValues: IWordRu;
} 

export interface IWordEnValue {
  id?: string;
  ownerId: string;
  Core: string; 
  WordEn: string;
  WordEnFrq: number;
  groupTagList: string;
  status?: string;
  createdAt?: Date;
  updatedAt?: Date;
  wordRus?: IRuDataValues[];
  translations?: string[];
  service?: number;
}

export interface IDataValues {
  dataValues: IWordEnValue;
} 

export interface IHintsData {
  wordEnId: string;
  wordRuId: string;
  hints: string[];
  HintRtg: number;
}

export interface ICommentsData {
  wordEnId: string;
  wordRuId: string;
  Comment: string;
  Usage: string;
  Index?: number;
}

export interface IEmptyData {
  userId: string,
  wordId: string,
}