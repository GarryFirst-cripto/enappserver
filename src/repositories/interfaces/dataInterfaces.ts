import { DataTypes } from "sequelize";

interface IstringField {
  allowNull: boolean; 
  type: DataTypes.StringDataTypeConstructor; 
  unique: boolean;
}

interface IMailField {
  [x: string]: any;
  allowNull: boolean; 
  type: DataTypes.StringDataTypeConstructor; 
  unique: boolean;
}

export interface IUserModel { 
  password: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  email: IMailField;
  sex: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }
  ownname: DataTypes.StringDataTypeConstructor; 
  age: DataTypes.StringDataTypeConstructor; 
  status: DataTypes.StringDataTypeConstructor; 
  verify: DataTypes.StringDataTypeConstructor; 
  point: DataTypes.NumberDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IWordEnModel {
  Core: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  WordEn: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  WordEnFrq: DataTypes.NumberDataTypeConstructor; 
  status: DataTypes.StringDataTypeConstructor;
  groupTagList: DataTypes.StringDataTypeConstructor;
  ownerId: DataTypes.StringDataTypeConstructor;
  service: DataTypes.NumberDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IWordRuModel {
  wordEnId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  WordRu: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  Transcription: DataTypes.StringDataTypeConstructor; 
  Meaning: DataTypes.StringDataTypeConstructor;
  MeaningProc: DataTypes.NumberDataTypeConstructor;
  ownerId: DataTypes.StringDataTypeConstructor;
  service: DataTypes.NumberDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IWordToWordModel {
  wordEnId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  wordRuId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IGroupTagModel {
  groupName: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  Info: DataTypes.StringDataTypeConstructor; 
  Index:  DataTypes.NumberDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface ICommentModel {
  wordEnId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  wordRuId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  Comment: DataTypes.StringDataTypeConstructor;
  Usage: DataTypes.StringDataTypeConstructor;
  Index: DataTypes.NumberDataTypeConstructor;
  Sex: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IHintModel {
  wordEnId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; };
  wordRuId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  Hint: DataTypes.StringDataTypeConstructor;
  HintRtg: DataTypes.NumberDataTypeConstructor;
  Sex: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface ILogModel {
  userId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  wordEnId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  wordRuId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  answer: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  wordstate: DataTypes.StringDataTypeConstructor; 
  wordzone: DataTypes.IntegerDataTypeConstructor;
  plane: DataTypes.DateDataTypeConstructor;
  timing: DataTypes.IntegerDataTypeConstructor;
  state: DataTypes.IntegerDataTypeConstructor;
  experience: DataTypes.IntegerDataTypeConstructor;
  mode: DataTypes.AbstractDataTypeConstructor;
  index: DataTypes.IntegerDataTypeConstructor; 
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IStatisticModel {
  userId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  experience: DataTypes.NumberDataTypeConstructor; 
  intensity: DataTypes.NumberDataTypeConstructor;
  status: DataTypes.StringDataTypeConstructor; 
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IMessageModel {
  userId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  mode: DataTypes.StringDataTypeConstructor;
  text: DataTypes.StringDataTypeConstructor;
  answer: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IMessReaction {
  userId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  messageId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  isLike: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; defaultValue: boolean; };
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IAudioModel {
  wordId: { allowNull: boolean; type: DataTypes.StringDataTypeConstructor; }; 
  language: DataTypes.StringDataTypeConstructor;
  voice: DataTypes.StringDataTypeConstructor;
  sex: DataTypes.StringDataTypeConstructor;
  publicLink: DataTypes.StringDataTypeConstructor;
  link: DataTypes.StringDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor;
}
