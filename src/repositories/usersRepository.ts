import { UserModel } from './Models/index';
import BaseRepository from './baseRepository';
import { IListFilter } from '../api/interfaces/interfaces';

class UserRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({ where, order: ['email'], offset, limit });
  }

  async getByEmail(mail: string) {
    if (mail) {
      const email = mail.toLowerCase();
      return await this.model.findOne({ where: { email } });
    }
    return null;
  }
 
}

export default new UserRepository(UserModel);
