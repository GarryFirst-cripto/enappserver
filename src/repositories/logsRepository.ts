import { Op } from 'sequelize';
import { LogModel, UserModel } from './Models/index';
import BaseRepository from './baseRepository';
import { IEmptyData } from  './interfaces/repoInterfaces';

class LogsRepository extends BaseRepository {

  async getList(filter: object) {
    return await this.model.findAll({ where: filter });
  }

  async getLogUserList(userId: string) {
    return await this.model.findAll({ where: {
      userId,
      mode: { [Op.not]: true },
      answer: { [Op.eq]: true }
    }});
  }

  async getFullList(filter: object) {
    return await this.model.findAll({
      where: filter,
      include: {
        model: UserModel
      }
    });
  }

  async lastUserLog(userId: string):Promise<any> {
    const result = await this.model.findAll({
      where: {
        userId,
        mode: { [Op.not]: true }
      },
      order: [['createdAt', 'desc']],
      offset: 0,
      limit: 1
    });
    return result;
  }

  async deleteUserLogs(userId: string) {
    const result = await this.model.destroy({ where: { userId } });
    return { result };
  }

  async deleteWordLogs(wordId: string) {
    const result = await this.model.destroy({ where: { wordEnId: wordId } });
    return { result };
  }

  async emptyLogs(data: IEmptyData) {
    const { userId, wordId } = data;
    const result = await this.model.destroy({ where: { userId, wordEnId: wordId } });
    return { result };
  }

}

export default new LogsRepository(LogModel);