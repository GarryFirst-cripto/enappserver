import { HintModel } from './Models/index';
import BaseRepository from './baseRepository';
import { IHintsData } from './interfaces/repoInterfaces';

class HintRepository extends BaseRepository {
  model: any;

  async getList(filter: object) {
    return await this.model.findAll({ where: filter });
  }

  async deleteWordHints(wordRuId: string) {
    const result = await this.model.destroy({ where: { wordRuId } });
    return { result };
  }

  async appendHints(hintData: IHintsData) {
    const { wordEnId, wordRuId, hints, HintRtg } = hintData;
    for (let i = 0; i < hints.length; i++) {
      if (hints[i]) {
        const Hint = hints[i];
        const oldItem = await this.model.findOne({ where : { wordEnId, wordRuId, Hint }});
        if (!oldItem) this.model.create({ wordEnId, wordRuId, Hint, HintRtg })
      }
    };
  }

}

export default new HintRepository(HintModel);