import Sequelize, { Op } from 'sequelize';
import { WordRuModel, WordToWordModel, WordEnModel, LogModel, CommentModel, HintModel, AudioModel } from './Models/index';
import BaseRepository from './baseRepository';
import HintRepository from './hintRepository';
import CommentRepository from './commentRepository';
import wordsRepository from './wordsRepository';
import { IWordDataRu } from './interfaces/repoInterfaces';
import { IListFilter,  IComplexFilter } from '../api/interfaces/interfaces';

  interface IErrorData {
    status: number;
    message: string;
    id?: string;
  }

// UPDATE "wordRus" SET "service" = 0;

class WordRuRepository extends BaseRepository {
  model: any;

  async createRuWord(data: any) {
    data.createdAt = new Date();
    data.service = '0';
    const { wordEnId, WordRu, Transcription } = data;
    try {
      const oldWord = await this.model.findOne({ where: { WordRu, Transcription } });
      const result = oldWord ? oldWord : await this.model.create(data);
      const wordRuId = result.id;
      const link = await WordToWordModel.findOne({ where: { wordEnId, wordRuId } });
      if (!link) {
        await WordToWordModel.create({ wordEnId, wordRuId });
      }
      if (data.hints) {
        const { hints } = data;
        await HintRepository.appendHints({ wordEnId, wordRuId, hints, HintRtg: 0 })
      }
    } catch (err) {
      return { status: 400, message: err }
    }
  }

  async updateById(id: string, data: any) {
    data.updatedAt = new Date();
    data.service = 0;
    delete data.id;
    try {
      const result = await this.model.update(data, {
        where: { id },
        returning: true,
        plain: true
      });
      return result[1];
    } catch (err) {
      return { status: 400, message: err }
    }
  }

  async getWordById(userId: string, id: string) {
    const result = await this.model.findOne({
      where: { id },
      attributes: {
        exclude: ['service', 'ownerId']
      },
      include: [{
        model: WordEnModel,
        required: false,
        order: [['WordEnFrq', 'DESK']],
        include: [{
          model: CommentModel,
          order: [['index', 'DESC']],
        },{
          model: HintModel,
          order: [['index', 'DESC']],
        },
        {
          model: LogModel,
          required: false,
          where: { userId },
          order: [['createdAt', 'DESC']],
        }]
      }]
    });
    return result;
  }

  async getWordsComplex(userId: string, userSex: string, filter: IComplexFilter) {
    const query = `(SELECT COUNT(0) FROM "loggs" WHERE "loggs"."wordRuId" = "wordRu"."id" and "loggs"."wordEnId" = "wordEn"."id" and "loggs"."userId" = '${userId}')`;
    const { from: offset, count: limit, minWeight, maxWeight } = filter;
    const minValue = minWeight ? minWeight * 100 : 0;
    const maxValue = maxWeight ? maxWeight * 100 : Number.MAX_SAFE_INTEGER;
    const list = await this.model.findOne({
      attributes: {
        exclude: ['ownerId', 'service'],
        include: [
          [Sequelize.literal('ROW_NUMBER() OVER (ORDER BY "WordEnFrq" * "MeaningProc")'), 'raw'],
        ]
      },
      order: [[ WordEnModel, Sequelize.literal('"WordEnFrq" * "MeaningProc"'), 'desc']],
      where: { 
        service: { [Op.eq]: (<number> <unknown>Sequelize.literal(query)) }
      },
      // where: { MeaningProc: { [Op.between]: [(<number> <unknown>Sequelize.literal(`${minValue} / "WordEnFrq"`)), (<number> <unknown>Sequelize.literal(`${maxValue} / "WordEnFrq"`))] }},
      include: [{
        model: WordEnModel,
        attributes: {
          exclude: ['service', 'ownerId'],
          include: [
            [Sequelize.literal('"MeaningProc" * "WordEnFrq" / 100'), 'weightField']
          ]
        },
        required: true,
        // order: [Sequelize.literal('"MeaningProc" * "WordEnFrq"')],
        where: { WordEnFrq: { [Op.between]: [(<number> <unknown>Sequelize.literal(`${minValue} / "MeaningProc"`)), (<number> <unknown>Sequelize.literal(`${maxValue} / "MeaningProc"`))] } },
        include: [ ... wordsRepository.dataInclude(userId, userSex), { model: AudioModel }]
      }],
      offset
      // limit
    });
    return list;
  }

  async getDataComplex(userId:string, filter: IComplexFilter) {
    const query = `(SELECT COUNT(0) FROM "loggs" WHERE "loggs"."wordRuId" = "wordRu"."id" and "loggs"."wordEnId" = "wordEn"."id" and "loggs"."userId" = '${userId}')`;
    const { count: limit, from: offset } = filter;
    const list = await this.model.findAll({
      attributes: {
        exclude: ['ownerId', 'service']
        // include: [
        //   [Sequelize.literal(query), 'raws'],
        // ]
      },
      order: [[ WordEnModel, Sequelize.literal('"WordEnFrq" * "MeaningProc"'), 'desc']],
      where: { service: { [Op.eq]: (<number> <unknown>Sequelize.literal(query)) }},
      include: [{
        model: WordEnModel,
        attributes: {
          exclude: ['service', 'ownerId'],
          include: [
            [Sequelize.literal('"MeaningProc" * "WordEnFrq" / 100'), 'weightField']
          ]
        },
        required: true
      }],
      offset,
      limit
    });
    return list;
  }

  async getDataCommon(userId: string) {
    // const query = `(SELECT COUNT(*) FROM "loggs" WHERE "loggs"."wordRuId" = "id" and "loggs"."userId" = '${userId}')`;
    const list = await this.model.findAll({
      attributes: ['id', 'WordRu', 'Transcription'],
      order: [[ WordEnModel, Sequelize.literal('"WordEnFrq" * "MeaningProc"'), 'desc']],
      // where: { service: { [Op.eq]: (<number> <unknown>Sequelize.literal(query)) }},
      include: [{
        model: WordEnModel,
        attributes: [
            'id',
            'WordEn',
            [Sequelize.literal('"MeaningProc" * "WordEnFrq" / 100'), 'weightField']
        ],
        required: true,
      },{
        model: LogModel,
        where: { userId },
        required: false
      }]
    });
    const result = list.filter((item: any) => (item.loggs.length === 0));
    return result;
  }

  async getSingleComplex(id:string) {
    const list = await this.model.findOne({
      attributes: {
        exclude: ['ownerId', 'service']
      },
      where: { id },
      include: [{
        model: WordEnModel,
        attributes: {
          exclude: ['service', 'ownerId', 'service'],
          include: [
            [Sequelize.literal('"MeaningProc" * "WordEnFrq" / 100'), 'weightField']
          ]
        },
        required: false
      }]
    });
    return list;
  }

  async delete(id: string) {
    await WordToWordModel.destroy({ where: { wordRuId: id }});
    await HintRepository.deleteWordHints(id);
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

  async getList(filter: IListFilter) {
    const { from: offset, count: limit } = filter;
    delete filter.from;
    delete filter.count;
    return await this.model.findAll({
      attributes: {
        exclude: ['ownerId']
      },
      where: filter,
      order: ['WordEn'],
      offset,
      limit
    });
  }

  async createRuWords(word: IWordDataRu):Promise<IWordDataRu | IErrorData> {
    const { wordEnId, ownerId, WordRu, Transcription, Meaning, MeaningProc, Hint1, Hint2, Hint3, HintRtg, Comment, Usage } = word;
    let oldWord = await this.model.findOne({ where: { ownerId } });
    if (oldWord) {
      await this.updateById(oldWord.id, { wordEnId, ownerId, WordRu, Transcription, Meaning, MeaningProc, service: 0 })
    } else { 
      oldWord = await this.model.findOne({ where: { WordRu } }); // , Transcription: 'xxxxx' } });
    };
    if (oldWord) {
      const { id: wordRuId } = oldWord;
      await CommentRepository.appendComment({ wordEnId, wordRuId, Comment, Usage });
      await HintRepository.appendHints({ wordEnId, wordRuId, hints: [Hint1, Hint2, Hint3], HintRtg })
      return oldWord;
    }
    const newWord = <IWordDataRu> <unknown> await this.create({ ...word, service: 0 });
    if (newWord) {
      const { id: wordRuId } = newWord;
      await CommentRepository.appendComment({ wordEnId, wordRuId, Comment, Usage, Index: 0 });
      await HintRepository.appendHints({ wordEnId, wordRuId, hints: [Hint1, Hint2, Hint3], HintRtg: 0 })
      return newWord;
    };
    return { status: 400, message: 'Some error occured ...' }
  }

  async getTranslations(limit:number, idArray:string[]) {
      const result = await this.model.findAll({
        where: { id: { [Sequelize.Op.notIn]: idArray } },
        order: [Sequelize.fn('RANDOM')],
        limit
      });
      return result;
  }

}

export default new WordRuRepository(WordRuModel);