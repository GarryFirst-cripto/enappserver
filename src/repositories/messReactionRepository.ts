import { MessReactionModel } from './Models/index';
import BaseRepository from './baseRepository';

class MessReactionRepository extends BaseRepository {
  
  async getMessReaction(userId:string, messageId:string) {
    return await this.model.findOne({ where: { userId, messageId } });
  }
  
  async deleteByMessage(messageId: string) {
    const result = await this.model.destroy({ where: { messageId } });
    return { result };
  }

}

export default new MessReactionRepository(MessReactionModel);