import { AudioModel } from './Models/index';
import BaseRepository from './baseRepository';
// import { IListFilter } from '../api/interfaces/interfaces';

class AudioRepository extends BaseRepository {
  model: any;
 
  async clearAudioList() {
    const result = await this.model.destroy({ where: {} });
    return { result };
  }

}

export default new AudioRepository(AudioModel);
