import { GroupTagModel } from './Models/index';
import BaseRepository from './baseRepository';

class GroupTagRepository extends BaseRepository {
  model: any;

}

export default new GroupTagRepository(GroupTagModel);