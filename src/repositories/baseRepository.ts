export default class BaseRepository {
  model: any;
  constructor(model: any) {
    this.model = model;
  }

  async getAll() {
    return this.model.findAll();
  }

  async getById(id: string) {
    return this.model.findByPk(id);
  }

  async create(data: any) {
    data.createdAt = data.createdAt || new Date();
    try {
      return await this.model.create(data);
    } catch (err) {
      return { status: 400, message: err }
    }
  }

  async updateById(id: string, data: any) {
    data.updatedAt = data.updatedAt || new Date();
    delete data.id;
    try {
      const result = await this.model.update(data, {
        where: { id },
        returning: true,
        plain: true
      });
      return result[1];
    } catch (err) {
      return { status: 400, message: err }
    }
  }

  async delete(id: string) {
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

}
