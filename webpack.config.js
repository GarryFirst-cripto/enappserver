const nodeExternals = require('webpack-node-externals')

module.exports = {
  entry: {
    server: './src/server.ts',
  },
  mode: "development",
  output: {
    filename: 'server.js',
    publicPath: '/dist/'
  },
  resolve: {
    extensions: ['.Webpack.js', '.web.js', '.ts', '.js', '.jsx', '.tsx']
  },
  externals: [nodeExternals()],
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 1024000
  },
  module: {
    rules: [
      {
        // Transpiles ES6-8 into ES5
        test: [/\.ts$/, /\.js$/],
        exclude: /node_modules/,
        use: {
          loader: "ts-loader"
        }
      }
    ]
  }
}